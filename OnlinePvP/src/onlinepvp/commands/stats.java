package onlinepvp.commands;

import java.util.UUID;

import onlinepvp.playerutil;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class stats implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String lab,String[] args) {
	if(cmd.getName().equalsIgnoreCase("stats")){
	if(cs instanceof Player){
	Player p = (Player) cs;	
    if(args.length == 0){
    p.sendMessage(ChatColor.GOLD + p.getDisplayName() + "'s stats");
    p.sendMessage(ChatColor.GOLD + "Deaths: " + playerutil.getInstance().getDeaths(p.getName()));
    p.sendMessage(ChatColor.GOLD + "Kills: " + playerutil.getInstance().getKills(p.getName()));
    p.sendMessage(ChatColor.GOLD + "KDR: " + playerutil.getInstance().getKDR(p.getName()));
    return false;	
    }
    if(args.length == 1){
    String name = args[0];
    UUID id = playerutil.getInstance().getUUID(name);
    if(id == null){
    p.sendMessage(ChatColor.GOLD + "Can't find data for " + name);
    return false;	
    }
    if(id !=null){
    p.sendMessage(ChatColor.GOLD + name + "'s stats");
    p.sendMessage(ChatColor.GOLD + "Deaths: " + playerutil.getInstance().getDeaths(name));
    p.sendMessage(ChatColor.GOLD + "Kills: " + playerutil.getInstance().getKills(name));
    p.sendMessage(ChatColor.GOLD + "KDR: " + playerutil.getInstance().getKDR(name));
    return false;	
    }
    }
	}	
	}
	return false;
	}

}
