package onlinepvp.commands;

import java.util.UUID;

import onlinepvp.playerutil;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class deaths implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String lab,String[] args) {
	if(cmd.getName().equalsIgnoreCase("deaths")){
	if(cs instanceof Player){
	Player p = (Player) cs;	
    if(args.length == 0){
    p.sendMessage(ChatColor.GOLD + "You have " + playerutil.getInstance().getDeaths(p.getName()) + " Deaths");
    return false;	
    }
    if(args.length == 1){
    String name = args[0];
    UUID id = playerutil.getInstance().getUUID(name);
    if(id == null){
    p.sendMessage(ChatColor.GOLD + "Can't find data for " + name);
    return false;	
    }
    if(id !=null){
    p.sendMessage(ChatColor.GOLD + name+" has " + playerutil.getInstance().getDeaths(name) +" Deaths");
    return false;	
    }
    }
	}	
	}
	return false;
	}

}
