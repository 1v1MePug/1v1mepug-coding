package onlinepvp;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class data {
	
private static data instance = new data();

public static data getInstance(){
return instance;	
}
	
	
public void sendKillData(CommandSender cs,int page,int removed,int keep){
ArrayList<String> dataz = this.getKillData();
Collections.sort(dataz, new Comparator<String>() {
public int compare(String a, String b) {
double aVal = Double.parseDouble(a.split(" ")[0]);
double bVal = Double.parseDouble(b.split(" ")[0]);
return Double.compare(aVal,bVal);
}
});
for(int i =0 ; i<removed; i++){
if(dataz.size() >i){
dataz.remove(i);	
}
}
cs.sendMessage(ChatColor.GOLD + "TopKills Page"+page);
for (int i = (dataz.size() > keep ? keep : dataz.size()) - 1; i >= 0; i--) {
String line = dataz.get(i);
String player = line.split(" ")[1], bal = line.split(" ")[0];
cs.sendMessage(ChatColor.GOLD+"#"+i+""+ChatColor.YELLOW + " "+ player + " : " + bal);
}		
}

public void sendDeathsData(CommandSender cs,int page,int removed,int keep){
ArrayList<String> dataz = this.getDeathsData();
Collections.sort(dataz, new Comparator<String>() {
public int compare(String a, String b) {
double aVal = Double.parseDouble(a.split(" ")[0]);
double bVal = Double.parseDouble(b.split(" ")[0]);
return Double.compare(aVal,bVal);
}
});
for(int i =0 ; i<removed; i++){
if(dataz.size() >i){
dataz.remove(i);	
}
}
cs.sendMessage(ChatColor.GOLD + "TopDeaths Page"+page);
for (int i = (dataz.size() > keep ? keep : dataz.size()) - 1; i >= 0; i--) {
String line = dataz.get(i);
String player = line.split(" ")[1], bal = line.split(" ")[0];
cs.sendMessage(ChatColor.GOLD+"#"+i+""+ChatColor.YELLOW + " "+ player + " : " + bal);
}		
}

public void sendKDRData(CommandSender cs,int page,int removed,int keep){
ArrayList<String> dataz = this.getKDRData();
Collections.sort(dataz, new Comparator<String>() {
public int compare(String a, String b) {
double aVal = Double.parseDouble(a.split(" ")[0]);
double bVal = Double.parseDouble(b.split(" ")[0]);
return Double.compare(aVal,bVal);
}
});
for(int i =0 ; i<removed; i++){
if(dataz.size() >i){
dataz.remove(i);	
}
}
cs.sendMessage(ChatColor.GOLD + "TopKDR Page"+page);
for (int i = (dataz.size() > keep ? keep : dataz.size()) - 1; i >= 0; i--) {
String line = dataz.get(i);
String player = line.split(" ")[1], bal = line.split(" ")[0];
cs.sendMessage(ChatColor.GOLD+"#"+i+""+ChatColor.YELLOW + " "+ player + " : " + bal);
}		
}


public ArrayList<String> getKillData(){
ArrayList<String> result = new ArrayList<String>();
try{
String table = fileutil.getInstance().getMYSQLVariable("table");
String query = "SELECT * FROM "+table;
PreparedStatement st = sqlutil.getInstance().c.prepareStatement(query);
ResultSet rs = st.executeQuery();
while(rs.next()){
String name = rs.getString(2);	
result.add(playerutil.getInstance().getKills(name) + " " + name);
}
}catch (Exception e){
e.printStackTrace();	
}
return result;
}

public ArrayList<String> getKDRData(){
ArrayList<String> result = new ArrayList<String>();
try{
String table = fileutil.getInstance().getMYSQLVariable("table");
String query = "SELECT * FROM "+table;
PreparedStatement st = sqlutil.getInstance().c.prepareStatement(query);
ResultSet rs = st.executeQuery();
while(rs.next()){
String name = rs.getString(2);	
result.add(playerutil.getInstance().getKDR(name) + " " + name);
}
}catch (Exception e){
	
}
return result;
}

public ArrayList<String> getDeathsData(){
ArrayList<String> result = new ArrayList<String>();
try{
String table = fileutil.getInstance().getMYSQLVariable("table");
String query = "SELECT * FROM "+table;
PreparedStatement st = sqlutil.getInstance().c.prepareStatement(query);
ResultSet rs = st.executeQuery();
String name = null;
while(rs.next()){
name = rs.getString(2);	
result.add(playerutil.getInstance().getDeaths(name) + " " + name);
}
}catch (Exception e){
	
}
return result;
}


public void sendTopKills(CommandSender cs,int page){
int keep = page * 10;
int remove = keep - 10;
this.sendKillData(cs,page,remove, keep);
}

public void sendTopDeaths(CommandSender cs,int page){
int keep = page * 10;
int remove = keep - 10;
this.sendDeathsData(cs,page,remove, keep);
}

public void sendTopKDR(CommandSender cs,int page){
int keep = page * 10;
int remove = keep - 10;
this.sendKDRData(cs,page,remove, keep);
}

}
