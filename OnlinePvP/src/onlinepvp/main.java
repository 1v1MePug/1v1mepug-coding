package onlinepvp;

import onlinepvp.commands.deaths;
import onlinepvp.commands.kdr;
import onlinepvp.commands.kills;
import onlinepvp.commands.stats;
import onlinepvp.commands.topdeaths;
import onlinepvp.commands.topkdr;
import onlinepvp.commands.topkills;
import onlinepvp.listeners.join;
import onlinepvp.listeners.leave;
import onlinepvp.listeners.pvp;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class main extends JavaPlugin{
	
	
public final join join = new join(); 
public final leave leave = new leave();
public final pvp pvp = new pvp();
	
public void onEnable(){
fileutil.getInstance().setup(getDataFolder());
sqlutil.getInstance().createConnection(this);
PluginManager pm = Bukkit.getServer().getPluginManager();
pm.registerEvents(this.join,this);
pm.registerEvents(this.leave,this);
pm.registerEvents(this.pvp,this);
getCommand("deaths").setExecutor(new deaths());
getCommand("kdr").setExecutor(new kdr());
getCommand("kills").setExecutor(new kills());
getCommand("stats").setExecutor(new stats());
getCommand("topkills").setExecutor(new topkills());
getCommand("topkdr").setExecutor(new topkdr());
getCommand("topdeaths").setExecutor(new topdeaths());
}

}
