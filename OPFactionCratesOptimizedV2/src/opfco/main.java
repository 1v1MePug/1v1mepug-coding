package opfco;

import java.util.ArrayList;

import opfco.commands.crates;
import opfco.commands.givekey;
import opfco.commands.resetcrates;
import opfco.listeners.playerlistener;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class main extends JavaPlugin{
	
public static ArrayList<cratesblock> crateblocks = new ArrayList<cratesblock>();
public static ArrayList<crateitem> crateitems = new ArrayList<crateitem>();

public final playerlistener pl = new playerlistener();

public void onEnable() {
fileutil.getInstance().setup(getDataFolder());
sqlutil.getInstance().createConnection(this);
PluginManager pm = Bukkit.getServer().getPluginManager();
pm.registerEvents(this.pl,this);
getCommand("crates").setExecutor(new crates());
getCommand("givekey").setExecutor(new givekey());
getCommand("resetcrates").setExecutor(new resetcrates());
saveDefaultConfig();
for (String str : getConfig().getKeys(false)) {
ConfigurationSection s = getConfig().getConfigurationSection(str);
ConfigurationSection l = s.getConfigurationSection("loc");
World w = Bukkit.getServer().getWorld(l.getString("world"));
double x = l.getDouble("x"), y = l.getDouble("y"), z = l.getDouble("z");
Location loc = new Location(w, x, y, z);
if (loc.getBlock() == null) {
getConfig().set(str, null);
} else {
crateblocks.add(new cratesblock(x,y,z,w));
}
}   

Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
public void run() {
for (cratesblock s : crateblocks) {
s.update();
}
}
}, 0, 30);


}


@Override
public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
Player p = (Player) sender;  
if (cmd.getName().equalsIgnoreCase("createcrate")) {
if(p.isOp()){
Block block = p.getTargetBlock(null, 10);
if (block == null) {
p.sendMessage(ChatColor.RED + "You are not looking at a chest!");
return true;
}
if (block.getType() != Material.CHEST && block.getType() !=Material.TRAPPED_CHEST) {
p.sendMessage(ChatColor.RED + "You are not looking at a chest!");
return true;
}
double x= block.getLocation().getX();
double y = block.getLocation().getY();
double z = block.getLocation().getZ();
cratesblock statusSign = new cratesblock(x,y,z,p.getWorld());
crateblocks.add(statusSign);
save(statusSign);
p.sendMessage(ChatColor.GOLD + "You have made a crate");
}
}       
return true;
}

public void save(cratesblock sign) {
int size = getConfig().getKeys(false).size() + 1;       
getConfig().set(size + ".loc.world", sign.getLocation().getWorld().getName());
getConfig().set(size + ".loc.x", sign.getLocation().getX());
getConfig().set(size + ".loc.y", sign.getLocation().getY());
getConfig().set(size + ".loc.z", sign.getLocation().getZ());
saveConfig();
}

}
