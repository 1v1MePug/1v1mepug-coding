package opfco;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;

public class fileutil {
	
private static fileutil instance = new fileutil();

public static fileutil getInstance(){
return instance;	
}

public File conf;
public File loot;

public void saveCrateItem(crateitem sign) {
FileConfiguration config = YamlConfiguration.loadConfiguration(loot);
int size = config.getKeys(false).size() + 1;    
config.set(size + ".keys.luck",sign.hasLuck());
config.set(size + ".keys.invitemname",sign.getInvItemName());
config.set(size+ ".keys.nameenabled",sign.hasEnabledName());
config.set(size + ".keys.name", sign.getName());
config.set(size + ".keys.materialid", sign.getMaterialID());
config.set(size + ".keys.materialamount", sign.getMaterialAmount());
config.set(size + ".keys.materialdata", sign.getMaterialData());
config.set(size + ".keys.maxchance",sign.getMaxChance());
config.set(size + ".keys.minchance",sign.getMinChance());
config.set(size + ".keys.ech1",sign.getFirstEnchant().getId());
config.set(size + ".keys.ech2",sign.getSecondEnchant().getId());
config.set(size + ".keys.ech3",sign.getThirdEnchant().getId());
config.set(size + ".keys.ech1level",sign.getFirstEnchantLevel());
config.set(size + ".keys.ech2level",sign.getSecondEnchantLevel());
config.set(size + ".keys.ech3level",sign.getThirdEnchantLevel());
try {
config.save(loot);
} catch (IOException e) {
}
}

public String getMYSQLVariable(String var){
FileConfiguration config = YamlConfiguration.loadConfiguration(conf);
return config.getString("mysql-"+var);
}

public void LoadLoot(){
FileConfiguration config = YamlConfiguration.loadConfiguration(loot);
for (String str : config.getKeys(false)) {
ConfigurationSection s = config.getConfigurationSection(str);
ConfigurationSection l = s.getConfigurationSection("keys");
boolean nameenabled = l.getBoolean("nameenabled");
String invitemname = l.getString("invitemname");
String name = l.getString("name");
int materialid = l.getInt("materialid");
int materialamount = l.getInt("materialamount");
short materialdata = (short)l.getInt("materialdata");
int chance = l.getInt("maxchance");
int chance2 = l.getInt("minchance");
Enchantment ech1 = Enchantment.getById(l.getInt("ech1"));
Enchantment ech2 = Enchantment.getById(l.getInt("ech2"));
Enchantment ech3 = Enchantment.getById(l.getInt("ech3"));
int ech1level = l.getInt("ech1level");
int ech2level = l.getInt("ech2level");
int ech3level = l.getInt("ech3level");
boolean luck = l.getBoolean("luck");
main.crateitems.add(new crateitem(invitemname,nameenabled,name,materialid,materialamount,materialdata,chance,ech1,ech2,ech3,ech1level,ech2level,ech3level,chance2,luck));
}   
}

public void setup(File dir){
if(!dir.exists()){
dir.mkdirs();	
}
loot = new File(dir + File.separator + "loot.yml");
if(!loot.exists()){
try{
loot.createNewFile();
crateitem irongolem = new crateitem("IronGolen Spawner",false,"IronGolemSpawner",52,1,(short)99,5,Enchantment.PROTECTION_ENVIRONMENTAL,Enchantment.PROTECTION_ENVIRONMENTAL,Enchantment.PROTECTION_ENVIRONMENTAL,0,0,0,1,true);
crateitem blaze = new crateitem("Blaze Spawner",false,"BlazeSpawner",52,1,(short)61,10,Enchantment.PROTECTION_ENVIRONMENTAL,Enchantment.PROTECTION_ENVIRONMENTAL,Enchantment.PROTECTION_ENVIRONMENTAL,0,0,0,5,true);
crateitem gapple = new crateitem("Gapple",false,"Gapple",322,16,(short)1,15,Enchantment.PROTECTION_ENVIRONMENTAL,Enchantment.PROTECTION_ENVIRONMENTAL,Enchantment.PROTECTION_ENVIRONMENTAL,0,0,0,10,true);
crateitem pug = new crateitem("1v1MePug's chestplate",false,"1v1MePug's chestplate",311,1,(short)0,20,Enchantment.PROTECTION_ENVIRONMENTAL,Enchantment.PROTECTION_FIRE,Enchantment.DURABILITY,4,4,3,15,true);
crateitem strenght = new crateitem("Strenght 2 potion",false,"Strenght 2 potion",373,6,(short)8233,25,Enchantment.PROTECTION_ENVIRONMENTAL,Enchantment.PROTECTION_ENVIRONMENTAL,Enchantment.PROTECTION_ENVIRONMENTAL,0,0,0,20,false);
crateitem speed = new crateitem("Speed 2 potion",false,"Speed 2 potion",373,6,(short)8226,35,Enchantment.PROTECTION_ENVIRONMENTAL,Enchantment.PROTECTION_ENVIRONMENTAL,Enchantment.PROTECTION_ENVIRONMENTAL,0,0,0,25,false);
crateitem sword = new crateitem("1v1MePug's sword",false,"1v1MePug's sword",276,1,(short)0,35,Enchantment.DAMAGE_ALL,Enchantment.DURABILITY,Enchantment.FIRE_ASPECT,5,3,2,50,false);
crateitem leggings = new crateitem("Protection 4 leggings",false,"Protection 4 leggings",312,1,(short)0,60,Enchantment.PROTECTION_ENVIRONMENTAL,Enchantment.PROTECTION_FIRE,Enchantment.DURABILITY,4,0,0,50,false);
crateitem chestplate = new crateitem("Protection 4 chestplate",false,"Protection 4 chestplate",311,1,(short)0,70,Enchantment.PROTECTION_ENVIRONMENTAL,Enchantment.PROTECTION_FIRE,Enchantment.DURABILITY,4,0,0,60,false);
crateitem boots = new crateitem("Protection 4 boots",false,"Protection 4 boots",313,1,(short)0,75,Enchantment.PROTECTION_ENVIRONMENTAL,Enchantment.PROTECTION_FIRE,Enchantment.DURABILITY,4,0,0,70,false);
crateitem helmet = new crateitem("Protection 4 helmet",false,"Protection 4 helmet",310,1,(short)0,80,Enchantment.PROTECTION_ENVIRONMENTAL,Enchantment.PROTECTION_FIRE,Enchantment.DURABILITY,4,0,0,75,false);
crateitem goldenappel = new crateitem("GoldenApple",false,"GoldenApple",322,16,(short)0,90,Enchantment.PROTECTION_ENVIRONMENTAL,Enchantment.PROTECTION_ENVIRONMENTAL,Enchantment.PROTECTION_ENVIRONMENTAL,0,0,0,80,false);
crateitem tnt = new crateitem("TNT",false,"TNT",46,64,(short)0,100,Enchantment.PROTECTION_ENVIRONMENTAL,Enchantment.PROTECTION_ENVIRONMENTAL,Enchantment.PROTECTION_ENVIRONMENTAL,0,0,0,90,false);
this.saveCrateItem(irongolem);
this.saveCrateItem(blaze);
this.saveCrateItem(gapple);
this.saveCrateItem(pug);
this.saveCrateItem(strenght);
this.saveCrateItem(speed);
this.saveCrateItem(sword);
this.saveCrateItem(leggings);
this.saveCrateItem(chestplate);
this.saveCrateItem(boots);
this.saveCrateItem(helmet);
this.saveCrateItem(goldenappel);
this.saveCrateItem(tnt);
}catch(Exception e){
}
}
conf = new File(dir + File.separator + "conf.yml");
if(!conf.exists()){
try{
FileConfiguration config = YamlConfiguration.loadConfiguration(conf);
config.set("mysql-ip","5.196.128.181");
config.set("mysql-port","3306");
config.set("mysql-user","root");
config.set("mysql-pass","supercode12mam");
config.set("mysql-database","PlayerData");
config.set("mysql-table","players");
config.save(conf);
}catch(Exception e){
}
}
this.LoadLoot();
}

}
