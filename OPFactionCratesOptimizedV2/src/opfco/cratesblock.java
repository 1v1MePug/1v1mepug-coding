package opfco;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

public class cratesblock {
	
public double x,y,z;

public World world;

public cratesblock(double x,double y,double z,World w){
this.x = x;
this.y = y;
this.z = z;
this.world = w;
}

public World getWorld(){
return this.world;	
}
public double getX(){
return this.x;	
}
public double getY(){
return this.y;	
}

public double getZ(){
return this.z;	
}

public Location getLocation(){
Location loc = new Location(world,x,y,z);
return loc;
}

public List<Location> getCylinder(Location center, double radiusX, double radiusZ, int height, boolean filled) {
    Vector pos = center.toVector();
    World world = center.getWorld();
    List<Location> blocks = new ArrayList<Location>();
    radiusX += 0.5;
    radiusZ += 0.5;

    if (height == 0) {
        return blocks;
    } else if (height < 0) {
        height = -height;
        pos = pos.subtract(new Vector(0, height, 0));
    }

    if (pos.getBlockY() < 0) {
        pos = pos.setY(0);
    } else if (pos.getBlockY() + height - 1 > world.getMaxHeight()) {
        height = world.getMaxHeight() - pos.getBlockY() + 1;
    }

    final double invRadiusX = 1 / radiusX;
    final double invRadiusZ = 1 / radiusZ;

    final int ceilRadiusX = (int) Math.ceil(radiusX);
    final int ceilRadiusZ = (int) Math.ceil(radiusZ);

    double nextXn = 0;
    forX: for (int x = 0; x <= ceilRadiusX; ++x) {
        final double xn = nextXn;
        nextXn = (x + 1) * invRadiusX;
        double nextZn = 0;
        forZ: for (int z = 0; z <= ceilRadiusZ; ++z) {
            final double zn = nextZn;
            nextZn = (z + 1) * invRadiusZ;

            double distanceSq = lengthSq(xn, zn);
            if (distanceSq > 1) {
                if (z == 0) {
                    break forX;
                }
                break forZ;
            }

            if (!filled) {
                if (lengthSq(nextXn, zn) <= 1 && lengthSq(xn, nextZn) <= 1) {
                    continue;
                }
            }

            for (int y = 0; y < height; ++y) {

                blocks.add(pos.add(new Vector(x,y,z)).toLocation(world));
                blocks.add(pos.add(new Vector(-x, y, z)).toLocation(world));
                blocks.add(pos.add(new Vector(x,y,-z)).toLocation(world));
                blocks.add(pos.add(new Vector(-x,y,-z)).toLocation(world));
            }
        }
    }

    return blocks;
}

private final double lengthSq(double x, double z) {
    return (x * x) + (z * z);
}

public void update(){
Location center = new Location(this.world,this.x,this.y,this.z);
for(Location loc : this.getCylinder(center,3,3,1,false)){
ParticleEffect.SPELL_WITCH.display(0, 0, 0, 1,20,loc,2);
ParticleEffect.CRIT_MAGIC.display(0, 0, 0, 1,20,loc,2);
ParticleEffect.CRIT_MAGIC.display(0, 0, 0, 1,20,loc,2);
ParticleEffect.LAVA.display(0, 0, 0, 1,20,loc,2);
ParticleEffect.LAVA.display(0, 0, 0, 1,20,loc,2);
ParticleEffect.LAVA.display(0, 0, 0, 1,20,loc,2);
}

}

}
