package opfco;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

public class playerutil {
	
	private static playerutil instance = new playerutil();
	
	public static playerutil getInstance(){
	return instance;	
	}
	
    public int getKeys(String Username){
    return this.getInt(3,Username);	
    }
    
    public UUID getUUID(String username){
    return UUID.fromString(this.getString(1,username));	
    }
    
    
    
    public void addKeys(String username,int keys){
    try{
    Statement st = sqlutil.getInstance().c.createStatement();
    String table = fileutil.getInstance().getMYSQLVariable("table");
    int key = this.getInt(3,username) + keys;
    st.executeUpdate("UPDATE "+table+" SET CrateKeys='"+key+"' WHERE Username='"+username+ "';");
    }catch (Exception e){    	
    }	
    }
    
    public void removeKeys(String username,int keys){
    try{
    Statement st = sqlutil.getInstance().c.createStatement();
    String table = fileutil.getInstance().getMYSQLVariable("table");
    int key = this.getInt(3,username) - keys;
    st.executeUpdate("UPDATE "+table+" SET CrateKeys='"+key+"' WHERE Username='"+username+ "';");
    }catch (Exception e){    	
    }	
    }
    
	

	public String getString(int value,UUID id){
	String table = fileutil.getInstance().getMYSQLVariable("table");
	String result = null;
	try {
	Statement statement = sqlutil.getInstance().c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE UUID ='" +id.toString()+ "';");
	res.next();
	result = res.getString(value);
	} catch (SQLException e){
	}
	return result;
	}
	
	public String getString(int value,String id){
	String table = fileutil.getInstance().getMYSQLVariable("table");
	String result = null;
	try {
	Statement statement = sqlutil.getInstance().c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE Username ='" +id.toString()+ "';");
	res.next();
	result = res.getString(value);
	} catch (SQLException e){
	}
	return result;
	}
	
	public int getInt(int value,String id){
	String table = fileutil.getInstance().getMYSQLVariable("table");
	int result = 0;
	try {
	Statement statement = sqlutil.getInstance().c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE Username ='" +id.toString()+ "';");
	res.next();
	result = res.getInt(value);
	} catch (SQLException e){
	}
	return result;
	}
	
	public int getInt(int value,UUID id){
	String table = fileutil.getInstance().getMYSQLVariable("table");
	int result = 0;
	try {
	Statement statement = sqlutil.getInstance().c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE UUID ='" +id.toString()+ "';");
	res.next();
	result = res.getInt(value);
	} catch (SQLException e){
	}
	return result;
	}
	
	public double getDouble(int value,UUID id){
	String table = fileutil.getInstance().getMYSQLVariable("table");
	double result = 0;
	try {
	Statement statement = sqlutil.getInstance().c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE UUID ='" +id.toString()+ "';");
	res.next();
	result = res.getDouble(value);
	} catch (SQLException e){
	}
	return result;
	}

}
