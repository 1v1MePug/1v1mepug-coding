package opfco.listeners;

import java.util.ArrayList;
import java.util.Random;

import opfco.crateitem;
import opfco.cratesblock;
import opfco.main;
import opfco.playerutil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public class playerlistener implements Listener{
	
public void runCrateKey(Player p){
Random r = new Random();
String username = p.getName();
int keys = 1;
PlayerInventory pi = p.getInventory();
int rand = r.nextInt(100) + 1;
for(crateitem crateitems : main.crateitems){
if(rand < crateitems.getMaxChance() && rand >=crateitems.getMinChance()){
playerutil.getInstance().removeKeys(username, keys);
pi.addItem(crateitems.getItem());
if(crateitems.hasLuck()){
Bukkit.broadcastMessage(ChatColor.DARK_RED + "["+ChatColor.GOLD+"NovaCrates"+ChatColor.DARK_RED+"] : " + ChatColor.GOLD + username + " just got " + crateitems.getInvItemName() + " out of a cratekey");	
}else{
p.sendMessage(ChatColor.DARK_RED + "["+ChatColor.GOLD+"NovaCrates"+ChatColor.DARK_RED+"] : " + ChatColor.GOLD + " you have got " + crateitems.getInvItemName() + " out of a cratekey");	
}

}
}
}

public ItemStack createItem(Material m,int amount,short dura,String tit){
ItemStack is = new ItemStack(m,amount,dura);
ItemMeta im = is.getItemMeta();
im.setDisplayName(tit);
is.setItemMeta(im);
return is;
}

public ItemStack createItem(Material m,int amount,String tit,Enchantment ec1,int ec1l){
ItemStack is = new ItemStack(m,amount);
ItemMeta im = is.getItemMeta();
im.setDisplayName(tit);
is.setItemMeta(im);
is.addEnchantment(ec1,ec1l);
return is;
}

public ItemStack createItem(Material m,int amount,String tit,Enchantment ec1,int ec1l,Enchantment ec2,int ec2l){
ItemStack is = new ItemStack(m,amount);
ItemMeta im = is.getItemMeta();
im.setDisplayName(tit);
is.setItemMeta(im);
is.addEnchantment(ec1,ec1l);
is.addEnchantment(ec2,ec2l);
return is;
}

public ItemStack createItem(Material m,int amount,String tit,Enchantment ec1,int ec1l,Enchantment ec2,int ec2l,Enchantment ec3,int ec3l){
ItemStack is = new ItemStack(m,amount);
ItemMeta im = is.getItemMeta();
im.setDisplayName(tit);
is.setItemMeta(im);
is.addEnchantment(ec1,ec1l);
is.addEnchantment(ec2,ec2l);
is.addEnchantment(ec3,ec3l);
return is;
}


public void createInv(Player p){
Inventory inv = Bukkit.createInventory(null,54,"NovaCrateLoot");
for(crateitem crateitems : main.crateitems){
ItemStack is = crateitems.getItem();
ItemMeta im = is.getItemMeta();
im.setDisplayName(crateitems.getInvItemName());
ArrayList<String> lore = new ArrayList<String>();
lore.add("Maximum Percentage : " + crateitems.getMaxChance());
lore.add("Minimum Percentage : " + crateitems.getMinChance());
lore.add("Luck : " + crateitems.hasLuck());
im.setLore(lore);
is.setItemMeta(im);
inv.addItem(is);
}
p.openInventory(inv);
}

@EventHandler
public void onInvClick(InventoryClickEvent event){
Inventory inv = event.getInventory();
if(inv.getTitle().equalsIgnoreCase("NovaCrateLoot")){
event.setCancelled(true);	
}
}
	
@EventHandler(ignoreCancelled = true,priority=EventPriority.MONITOR)
public void onInteract(PlayerInteractEvent event){
Player p = (Player) event.getPlayer();
if(event.getClickedBlock().getType() == Material.CHEST || event.getClickedBlock().getType() == Material.TRAPPED_CHEST){
String username = p.getName();
int keys = playerutil.getInstance().getKeys(username);
Location chestloc = event.getClickedBlock().getLocation();
double x = chestloc.getX();
double y = chestloc.getY();
double z = chestloc.getZ();
if(event.getAction() == Action.RIGHT_CLICK_BLOCK){
for(cratesblock cb : main.crateblocks){
if(x == cb.x && y == cb.y && z == cb.z){
this.createInv(p);
event.setUseInteractedBlock(Result.DENY);
return;			
}
}
}
if(event.getAction() == Action.LEFT_CLICK_BLOCK){
for(cratesblock cb : main.crateblocks){
if(x == cb.x && y == cb.y && z == cb.z){
if(keys == 0){
p.sendMessage(ChatColor.GOLD + "You need a cratekey to start");
event.setUseInteractedBlock(Result.DENY);
return;	
}
if(keys >0){
this.runCrateKey(p);	
event.setUseInteractedBlock(Result.DENY);
return;
}
}
}
}
}
}


}
