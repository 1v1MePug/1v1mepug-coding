package opfco.commands;

import java.util.UUID;

import opfco.playerutil;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class resetcrates implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String lab,String[] args) {
	if(cmd.getName().equalsIgnoreCase("resetcrates")){
	if(cs instanceof Player){
	Player p = (Player) cs;
	if(p.isOp()){
    if(args.length == 0){
    p.sendMessage(ChatColor.GOLD + "Resetted your crates");
    playerutil.getInstance().removeKeys(p.getName(),playerutil.getInstance().getKeys(p.getName()));
    return false;	
    }
    if(args.length == 1){
    String name = args[0];
    UUID id = playerutil.getInstance().getUUID(name);
    if(id == null){
    p.sendMessage(ChatColor.GOLD + "Can't find data for " + name);
    return false;	
    }
    if(id !=null){
    p.sendMessage(ChatColor.GOLD + "Resetted " + name + "'s keys");
    playerutil.getInstance().removeKeys(name,playerutil.getInstance().getKeys(name));
    return false;	
    }
    }
    }
	}	
	}
	return false;
	}

}
