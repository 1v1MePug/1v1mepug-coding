package opfco.commands;

import java.util.UUID;

import opfco.playerutil;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class crates implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String lab,String[] args) {
	if(cmd.getName().equalsIgnoreCase("crates")){
	if(cs instanceof Player){
	Player p = (Player) cs;	
    if(args.length == 0){
    p.sendMessage(ChatColor.GOLD + "You have " + playerutil.getInstance().getKeys(p.getName()) + " CrateKeys");
    return false;	
    }
    if(args.length == 1){
    String name = args[0];
    UUID id = playerutil.getInstance().getUUID(name);
    if(id == null){
    p.sendMessage(ChatColor.GOLD + "Can't find data for " + name);
    return false;	
    }
    if(id !=null){
    p.sendMessage(ChatColor.GOLD + name+" has " + playerutil.getInstance().getKeys(name) +" CrateKeys");
    return false;	
    }
    }
	}	
	}
	return false;
	}

}
