package opfco;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class crateitem {
	
private String invitemname;
private String name;
private int materialid;
private int materialamount;
private short materialdata;
private boolean enabledname;

private boolean lucky;

private Enchantment ec1;
private Enchantment ec2;
private Enchantment ec3;

private int ec1level;
private int ec2level;
private int ec3level;

private int maxchance;
private int minchance;

public crateitem(String invitemname,boolean enablename,String name,int materialid,int materialamount,short materialdata,int maxchance,Enchantment ec1,Enchantment ec2,Enchantment ec3,int ec1lev,int ec2lev,int ec3lev,int minchance,boolean luck){
this.invitemname = invitemname;
this.enabledname = enablename;
this.name = name;
this.materialid = materialid;
this.materialamount = materialamount;
this.materialdata = materialdata;
this.maxchance = maxchance;
this.ec1 = ec1;
this.ec2 = ec2;
this.ec3 = ec3;
this.ec1level = ec1lev;
this.ec2level = ec2lev;
this.ec3level = ec3lev;
this.minchance = minchance;
this.lucky = luck;
}

public boolean hasLuck(){
return this.lucky;	
}

public ItemStack getItem(){
ItemStack is = new ItemStack(this.materialid,this.materialamount,this.materialdata);
if(this.hasEnabledName()){
ItemMeta im = is.getItemMeta();
im.setDisplayName(name);
is.setItemMeta(im);
}
if(this.getFirstEnchantLevel() >=1){
is.addEnchantment(this.getFirstEnchant(),this.getFirstEnchantLevel());	
}
if(this.getSecondEnchantLevel() >=1){
is.addEnchantment(this.getSecondEnchant(),this.getSecondEnchantLevel());	
}
if(this.getThirdEnchantLevel() >=1){
is.addEnchantment(this.getThirdEnchant(),this.getThirdEnchantLevel());	
}
return is;
}

public boolean hasEnabledName(){
return this.enabledname;	
}

public String getInvItemName(){
return this.invitemname;	
}

public Enchantment getFirstEnchant(){
return this.ec1;
}
public Enchantment getSecondEnchant(){
return this.ec2;	
}
public Enchantment getThirdEnchant(){
return this.ec3;	
}
public int getFirstEnchantLevel(){
return this.ec1level;	
}
public int getSecondEnchantLevel(){
return this.ec2level;	
}
public int getThirdEnchantLevel(){
return this.ec3level;	
}


public int getMaxChance(){
return this.maxchance;	
}

public int getMinChance(){
return this.minchance;	
}

public String getName(){
return this.name;	
}
public int getMaterialID(){
return this.materialid;	
}
public int getMaterialAmount(){
return this.materialamount;	
}
public short getMaterialData(){
return this.materialdata;	
}

}
