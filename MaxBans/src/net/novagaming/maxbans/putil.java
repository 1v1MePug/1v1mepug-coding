package net.novagaming.maxbans;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public class putil {
	
	public static File userdir; 
	
	public static void getBanList(CommandSender cs){
	StringBuilder str = new StringBuilder();
	for(File file : new File(userdir.getPath()).listFiles()){
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);
	if(config.getBoolean("banned")){
	str.append(config.getString("lastname")+ " ");	
	}
	}
	if(str.length() == 0){
	cs.sendMessage(ChatColor.GOLD + "Banned Players : None");	
	}
	if(str.length() >1){
	cs.sendMessage(ChatColor.GOLD + "Banned Players : " + str.toString());	
	}
	}
	
	public static void dupeip(CommandSender cs,String ip){
	StringBuilder builder = new StringBuilder();
	String iptocheck = null;
	for(File file : new File(userdir.getPath()).listFiles()){
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);
	if(config.getString("lastname").equalsIgnoreCase(ip)){
	iptocheck = config.getString("ip");
	}
	}
	for(File tocheck : new File(userdir.getPath()).listFiles()){
	FileConfiguration config = YamlConfiguration.loadConfiguration(tocheck);
	if(config.getString("ip").equalsIgnoreCase(iptocheck));
	builder.append(config.getString("lastname"));	
	}
	if(builder.toString().length() == 0){
	cs.sendMessage(ChatColor.GOLD + "No alternative accounts found");	
	}
	if(builder.toString().length() >1){
	cs.sendMessage(ChatColor.GOLD + "Accounts found : " + builder.toString());
	}
	}
	
	public static boolean isBanned(UUID id){
	File file = new File(userdir + File.separator + id.toString() + ".yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);	
	return config.getBoolean("banned");
	}
	
	public static boolean isTempBanned(UUID id){
	File file = new File(userdir + File.separator + id.toString() + ".yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);	
	return config.getBoolean("tempban");
	}
	
	public static String getBanReason(UUID id){
	File file = new File(userdir + File.separator + id.toString() + ".yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);
	if(!config.contains("banreason")){
	return config.getString("No ban found");	
	}
	return config.getString("banreason");
	}
	
	public static long getBanTime(UUID id){
	File file = new File(userdir + File.separator + id.toString() + ".yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);
	return config.getLong("bantime");
	}
	public static String getName(String name){
	for(File file : new File(userdir.getPath()).listFiles()){
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);
	if(config.getString("lastname").equalsIgnoreCase(name)){
	return config.getString("lastname");	
	}
	}
	return null;
	}
	
	public static String getWarn(int i,UUID id){
	File file = new File(userdir + File.separator + id.toString() + ".yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);
	String result;
	if(config.getString("warning"+i) == null){	
	result = "No warning";
	}else{
	result = config.getString("warning"+i) + " warned by " + config.getString("warngiver"+i);
	}
	return result;
	}
	
	public static String getBanner(UUID id){
	File file = new File(userdir + File.separator + id.toString() + ".yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);
	return config.getString("bangiver");
	}
	
	public static void warn(UUID id,String towarn,String reason,String sender){
	File file = new File(userdir + File.separator + id.toString() + ".yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);
	int warns = config.getInt("warns") + 1;
	config.set("warns",warns);
	config.set("warning"+warns,reason);
	config.set("warngiver"+warns,sender);
	try {
	Bukkit.broadcastMessage(ChatColor.GOLD + towarn + " got warned by : " + sender + " for " + reason);
	config.save(file);
	} catch (IOException e) {
	e.printStackTrace();
	}
	}
	
	public static void unwarn(UUID id,String towarn,String sender){
	File file = new File(userdir + File.separator + id.toString() + ".yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);
	int warns = config.getInt("warns") - 1;
	config.set("warns",warns);
	config.set("warning"+warns,null);
	config.set("warngiver"+warns,null);
	try {
	Bukkit.broadcastMessage(ChatColor.GOLD + towarn + " got a warn pardoned by : " + sender);
	config.save(file);
	} catch (IOException e) {
	e.printStackTrace();
	}
	}
	
	public static void clearwarn(UUID id,String towarn){
	File file = new File(userdir + File.separator + id.toString() + ".yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);
	int warns = config.getInt("warns");
	for(int i = 0; i<warns; i++){
	config.set("warning"+i,null);
	config.set("warngiver"+i,null);
	}
	try {
	config.set("warns",0);
	config.save(file);
	} catch (IOException e) {
	e.printStackTrace();
	}
	}
	
	
	public static int getWarns(UUID id){
	File file = new File(userdir + File.separator + id.toString() + ".yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);
	return config.getInt("warns");
	}
	
	public static boolean checkBansonIp(String ip){
	for(File file : new File(userdir.getPath()).listFiles()){
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);
	if(config.getString("ip").equalsIgnoreCase(ip)){
	if(config.getBoolean("banned")){
	return true;	
	}
	}
	}
	return false;
	}
	
	public static void addBan(UUID id,String reason,String sender){
	File file = new File(userdir + File.separator + id.toString() + ".yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);
	config.set("banreason",reason);
	config.set("banned",true);
	config.set("tempban",false);
	config.set("bangiver",sender);
	try {
	config.save(file);
	} catch (IOException e) {
	e.printStackTrace();
	}
	}
	public static void addTempBan(UUID id,String reason,long bantime,String sender){
	File file = new File(userdir + File.separator + id.toString() + ".yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);
	config.set("banreason",reason);
	config.set("banned",true);
	config.set("tempban",true);
	config.set("bantime",System.currentTimeMillis() + bantime);
	config.set("bangiver",sender);
	try {
	config.save(file);
	} catch (IOException e) {
	e.printStackTrace();
	}
	}
	
	public static void removeBan(UUID id){
	File file = new File(userdir + File.separator + id.toString() + ".yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);
	config.set("banreason",null);
	config.set("banned",null);
	config.set("tempban",null);
	config.set("bantime",null);
	config.set("bangiver",null);
	try {
	config.save(file);
	} catch (IOException e) {
	e.printStackTrace();
	}
	}
	
	public static UUID getPlayer(String name){
	for(File file : new File(userdir.getPath()).listFiles()){
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);	
	if(config.getString("lastname").equalsIgnoreCase(name)){
	UUID id = UUID.fromString(config.getString("uuid"));
	return id;
	}
	}
	return null;
	}
	
	public static long getMuteTime(UUID id){
	File file = new File(userdir + File.separator + id.toString() + ".yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);	
	return config.getLong("mutetime");
	}
	
	public static boolean isMuted(UUID id){
	File file = new File(userdir + File.separator + id.toString()+ ".yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);		
	return config.getBoolean("muted");
	}
	public static void addMute(UUID id,String who,String reason,String sender,long time){
	File file = new File(userdir +File.separator +  id.toString() + ".yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);
	config.set("muted",true);
	config.set("mutetime",System.currentTimeMillis()+time);
	try {
	Bukkit.broadcastMessage(ChatColor.GOLD + sender + " muted " + who + " for : " + reason);
	config.save(file);
	} catch (IOException e) {
	e.printStackTrace();
	}
	System.out.println(config.getBoolean("muted"));
	}
	public static void removeMute(UUID id,String who,String sender){
	File file = new File(userdir + File.separator + id.toString() +".yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);
	config.set("muted",null);
	config.set("mutetime",null);
	try {
	Bukkit.broadcastMessage(ChatColor.GOLD + who + " got unmuted by " + sender);
	config.save(file);
	} catch (IOException e) {
	e.printStackTrace();
	}
	}
	
	
	
	public static boolean isRegistered(UUID id){
	File file = new File(userdir + File.separator + id.toString() + ".yml");	
	return file.exists();
	}
	
	public static UUID getBanNameOnIP(String ip){
	for(File file : new File(userdir.getPath()).listFiles()){
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);
	if(config.getString("ip").equalsIgnoreCase(ip)){
	return UUID.fromString(config.getString("uuid"));	
	}
	}
	return null;
	}
	
	public static void updatePlayer(UUID id,String lastname,String ip){
	File file = new File(userdir + File.separator + id.toString() + ".yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);
	config.set("lastname",lastname);
	config.set("ip",ip);
	try {
	config.save(file);
	} catch (IOException e) {
	e.printStackTrace();
	}
	}
	
	public static void registerPlayer(UUID id,String lastname,String ip){
	File file = new File(userdir + File.separator + id.toString() + ".yml");
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);
	config.set("lastname",lastname);
	config.set("uuid",id.toString());
	config.set("ip", ip.replaceAll("/",""));
	try {
	config.save(file);
	} catch (IOException e) {
	e.printStackTrace();
	}
	}
	
	public static void setup(Plugin p){
	if(!p.getDataFolder().exists()){
	p.getDataFolder().mkdirs();	
	}
	userdir = new File(p.getDataFolder() + File.separator + "PlayerCache");
	if(!userdir.exists()){
	userdir.mkdirs();	
	}
	}
	
	

}
