package net.novagaming.maxbans;

import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class unmute implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String lab,String[] args) {
    if(cmd.getName().equalsIgnoreCase("unmute")){
    if(!cs.hasPermission(permissions.unmute)){
    cs.sendMessage(ChatColor.RED + "You don't have permission for this command if you believe this is an error contact the server admins");
    return false;	
    }
    if(cs.hasPermission(permissions.unmute)){
    if(args.length < 1){
    cs.sendMessage(ChatColor.RED + "/unmute <name>");
    return false;	
    }
    UUID id  = putil.getPlayer(args[0]);
    putil.removeMute(id,args[0],cs.getName());
    }
    }
	return false;
	}

}
