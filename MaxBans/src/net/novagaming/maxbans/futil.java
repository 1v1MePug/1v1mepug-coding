package net.novagaming.maxbans;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class futil {
	
	public static File conf;
	public static File ipban;
	
	public static int getInt(String what){
	FileConfiguration config = YamlConfiguration.loadConfiguration(conf);
	return config.getInt(what);
	}
	
	public static void removeIP(String ip){
	FileConfiguration config = YamlConfiguration.loadConfiguration(ipban);
	config.set(ip,null);
	try {
	config.save(ipban);
	} catch (IOException e) {
	e.printStackTrace();
	}
	}
	
	public static boolean isIPbanned(String ip){
	FileConfiguration config = YamlConfiguration.loadConfiguration(ipban);
	return config.contains(ip);
	}
	
	public static void addIP(String ip){
	FileConfiguration config = YamlConfiguration.loadConfiguration(ipban);
	config.set(ip,ip);
	try {
	config.save(ipban);
	} catch (IOException e) {
	e.printStackTrace();
	}
	}
	
	
	public static void setup(File dir){
	if(!dir.exists()){
	dir.mkdirs();	
	}
	ipban = new File(dir + File.separator + "ipbans.yml");
	if(!ipban.exists()){
	try {
	ipban.createNewFile();
	} catch (IOException e) {
	e.printStackTrace();
	}	
	}
	conf = new File(dir + File.separator + "conf.yml");	
	if(!conf.exists()){
	FileConfiguration config = YamlConfiguration.loadConfiguration(conf);
	config.set("maxwarnings",5);
	try {
	config.save(conf);
	} catch (IOException e) {
	e.printStackTrace();
	}
	}
	
	}

}
