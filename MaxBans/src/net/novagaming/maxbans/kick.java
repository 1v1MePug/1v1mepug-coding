package net.novagaming.maxbans;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class kick implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String lab,String[] args) {
    if(cmd.getName().equalsIgnoreCase("kick")){
    if(!cs.hasPermission(permissions.kick)){
    cs.sendMessage(ChatColor.RED + "You don't have permission for this command if you believe this is an error contact the server admins");
    return false;	
    }
    if(cs.hasPermission(permissions.kick)){
    if(args.length <1){
    cs.sendMessage(ChatColor.RED + "/kick <name>");
    return false;	
    }
    StringBuilder str = new StringBuilder();
    for(int i = 1; i<args.length; i++){
    str.append(args[i] + " ");	
    }
    Player target = (Player) Bukkit.getServer().getPlayer(args[0]);
    if(target !=null){
    target.kickPlayer(ChatColor.GOLD + cs.getName() + " Kicked you for : " + str.toString());	
    Bukkit.broadcastMessage(ChatColor.GOLD + args[0] + " was kicked for " + str.toString() + " by : " + cs.getName());
    }
    }
    }
	return false;
	}

}
