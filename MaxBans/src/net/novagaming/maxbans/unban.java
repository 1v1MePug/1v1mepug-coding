package net.novagaming.maxbans;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class unban implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String lab,String[] args) {
    if(cmd.getName().equalsIgnoreCase("unban")){
    if(!cs.hasPermission(permissions.unban)){
    cs.sendMessage(ChatColor.RED + "You don't have permission for this command if you believe this is an error contact the server admins");
    return false;	
    }
    if(cs.hasPermission(permissions.unban)){
    if(args.length <1){
    cs.sendMessage(ChatColor.RED + "/unban <name>");
    return false;	
    }
    UUID id  = putil.getPlayer(args[0]);
    Bukkit.broadcastMessage(ChatColor.GOLD + args[0] + " was unbanned by : " + cs.getName());
    putil.removeBan(id);
    }
    }
	return false;
	}

}
