package net.novagaming.maxbans;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class warn implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String lab,String[] args) {
	if(cmd.getName().equalsIgnoreCase("warn")){
	if(!cs.hasPermission(permissions.warn)){
	cs.sendMessage(ChatColor.RED + "You don't have permission for this command if you believe this is an error contact the server admins");
	return false;	
	}
	if(cs.hasPermission(permissions.warn)){
	if(args.length < 2){
	cs.sendMessage(ChatColor.RED + "/warn <name> <reason>");
	return false;	
	}
	StringBuilder str = new StringBuilder();
	for(int i = 1; i<args.length; i++){
	str.append(args[i] + " ");	
	}
    Player p = (Player) Bukkit.getPlayer(args[0]);
    if(p !=null){
    if(p.isOp()){
    cs.sendMessage(ChatColor.GOLD + "Can't warn this person");
    return false;	
    }
    }
    UUID id = putil.getPlayer(args[0]);
    if(id !=null){
    putil.warn(id,args[0],str.toString(),cs.getName());
    int warns = putil.getWarns(id);
    if(warns >= futil.getInt("maxwarnings")){
    int time = 1;
    time = time * 60 * 60;
    putil.addTempBan(id,str.toString(),time * 1000,cs.getName());	
    Bukkit.broadcastMessage(ChatColor.GOLD +cs.getName() + "Tempbanned : " + args[0] + " For : Max warnings [1Hour tempban]");
    putil.clearwarn(id,args[0]);
    }
    return false;	
    }
	}
	}
	return false;
	}

}
