package net.novagaming.maxbans;

import java.net.InetSocketAddress;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;

public class playerlistener implements Listener{
	
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event){
	Player p = (Player) event.getPlayer();
	UUID id = p.getUniqueId();
	boolean registered = putil.isRegistered(id);
	if(!registered){
	InetSocketAddress addres = p.getAddress();
	String ip = addres.getAddress().getAddress().toString();
	putil.registerPlayer(id,p.getName(),ip.replaceAll("/",""));	
	}
	}
	
	@EventHandler
	public void onLeave(PlayerQuitEvent event){
	Player p = (Player) event.getPlayer();
	UUID id = p.getUniqueId();
	String ip = p.getAddress().toString().split("/")[(p.getAddress().toString().split("/").length)-1].split(":")[0];
	putil.updatePlayer(id,p.getName(),ip.replaceAll("/",""));
	}
	@EventHandler
	public void scanip(PlayerLoginEvent event){
	Player p = (Player) event.getPlayer();
	UUID id = p.getUniqueId();
	String ip = event.getAddress().getHostName().toString().replaceAll("/","");	
	if(futil.isIPbanned(ip) && !putil.isBanned(id)){
	putil.addBan(id,"Ban evading","IKillYouHue");
	Bukkit.broadcastMessage(ChatColor.GOLD +p.getName() + " has been banned for ban evading");
	event.disallow(Result.KICK_BANNED,ChatColor.RED + "You have been banned for ban evading");	
	return;
	}
	if(futil.isIPbanned(ip) && putil.isBanned(id)){
	event.disallow(Result.KICK_BANNED,ChatColor.RED+"Your ip is banned if you think this is unfair appeal @ novagaming.co.uk/Forum");
	}
	}
	
	@EventHandler(ignoreCancelled = true,priority=EventPriority.HIGHEST)
	public void onChat(AsyncPlayerChatEvent event){
	Player p = (Player) event.getPlayer();
	UUID id = p.getUniqueId();
	boolean muted = putil.isMuted(id);
	if(muted){
	long time = putil.getMuteTime(id);
	if(System.currentTimeMillis() >=time){
    putil.removeMute(id,p.getName(),"CONSOLE");
	}else{
	Calendar c = Calendar.getInstance();
	c.setTimeInMillis(time);
	String date = now("dd-MM-yyyy hh-mm-ss",c);
	p.sendMessage(ChatColor.RED + "You are still muted");
	p.sendMessage(ChatColor.RED + "You will be unmuted at : " + date);
	event.setCancelled(true);	
	}
	}
	}
	
	public static String now(String dateFormat,Calendar cal) {
    SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
	return sdf.format(cal.getTime());
    }
	
	@EventHandler
	public void onLogin(PlayerLoginEvent event){
	Player p = (Player) event.getPlayer();
	UUID id = p.getUniqueId();
	if(putil.isRegistered(id)){
	if(putil.isBanned(id)){
	if(putil.isTempBanned(id)){
	long ms = putil.getBanTime(id);
	if(System.currentTimeMillis() >=ms){
	putil.removeBan(id);	
	}else{
	Calendar c = Calendar.getInstance();
	c.setTimeInMillis(ms);
	String date = c.getTime().toString();
	event.disallow(Result.KICK_BANNED,"BanReason : " + putil.getBanReason(id) + "\n" + ChatColor.GOLD + "Your ban will be removed on " + date + "\n If you think this ban isnt valid go to novagaming.co.uk/forum" + ChatColor.WHITE + "" + ChatColor.BOLD +  "\n Banned by " + putil.getBanner(id));	
	}
	}else{
	event.disallow(Result.KICK_BANNED,putil.getBanReason(id) + "\n If you think this ban isnt valid go to novagaming.co.uk/forum" + ChatColor.WHITE + "" + ChatColor.BOLD +  "\n Banned by " + putil.getBanner(id));	
	}
	}
	}
	}

}
