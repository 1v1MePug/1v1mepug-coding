package net.novagaming.maxbans;

import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class mute implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String lab,String[] args) {
    if(cmd.getName().equalsIgnoreCase("mute")){
    if(!cs.hasPermission(permissions.mute)){
    cs.sendMessage(ChatColor.RED + "You don't have permission for this command if you believe this is an error contact the server admins");
    return false;	
    }
    if(cs.hasPermission(permissions.mute)){
    if(args.length < 3){
    cs.sendMessage(ChatColor.RED + "/mute <name> <timeformat:time> <reason>");
    return false;	
    }
    UUID id  = putil.getPlayer(args[0]);
    String [] split = args[1].split(":");
    String format = split[0];
    int time = Integer.parseInt(split[1]);
    StringBuilder str = new StringBuilder();
    if ((format.equalsIgnoreCase("seconds")) || (format.equalsIgnoreCase("s"))) {
    time += 0;
    }
    if ((format.equalsIgnoreCase("minutes")) || (format.equalsIgnoreCase("min"))) {
    time *= 60;
    }
    if ((format.equalsIgnoreCase("hours")) || (format.equalsIgnoreCase("h"))) {
    time = time * 60 * 60;
    }
    if ((format.equalsIgnoreCase("days")) || (format.equalsIgnoreCase("d"))) {
    time = time * 60 * 60 * 24;
    }
    if ((format.equalsIgnoreCase("weeks")) || (format.equalsIgnoreCase("w"))) {
    time = time * 60 * 60 * 24 * 7;
    }
    if ((format.equalsIgnoreCase("months")) || (format.equalsIgnoreCase("m"))) {
    time = time * 60 * 60 * 24 * 7 * 30;
    }
    for(int i = 2; i<args.length; i++){
    str.append(args[i] + " ");	
    }
    putil.addMute(id,args[0],str.toString(),cs.getName(), time * 1000);
    }
    }
	return false;
	}

}
