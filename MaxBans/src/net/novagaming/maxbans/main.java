package net.novagaming.maxbans;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class main extends JavaPlugin {
	
	public final playerlistener pl = new playerlistener();
	
	
	public void onEnable(){
	PluginManager pm = Bukkit.getServer().getPluginManager();
	pm.registerEvents(this.pl,this);
	putil.setup(this);
	futil.setup(getDataFolder());
	permissions.registerPerms(pm);
	getCommand("ban").setExecutor(new ban());
	getCommand("banip").setExecutor(new banip());
	getCommand("banlist").setExecutor(new banlist());
	getCommand("checkban").setExecutor(new checkban());
	getCommand("dupeip").setExecutor(new dupeip());
	getCommand("kick").setExecutor(new kick());
	getCommand("mute").setExecutor(new mute());
	getCommand("rangebanip").setExecutor(new rangebanip());
	getCommand("tempban").setExecutor(new tempban());
	getCommand("unban").setExecutor(new unban());
	getCommand("unbanip").setExecutor(new unbanip());
	getCommand("unmute").setExecutor(new unmute());
	getCommand("unrangebanip").setExecutor(new unrangebanip());
	getCommand("unwarn").setExecutor(new unwarn());
	getCommand("warn").setExecutor(new warn());

	}

}
