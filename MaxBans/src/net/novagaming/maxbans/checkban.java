package net.novagaming.maxbans;

import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class checkban implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String lab,String[] args) {
	if(cmd.getName().equalsIgnoreCase("checkban")){
	if(!(cs instanceof Player)){
	if(args.length == 0){
	cs.sendMessage(ChatColor.RED + "/checkban <name>");
	return false;	
	}
	if(args.length == 1){
	String name = putil.getName(args[0]);
	UUID id = putil.getPlayer(name);	
	if(id !=null){
	cs.sendMessage(ChatColor.GOLD + name + " Warnings / Bans");
	for(int i = 1; i<6; i++){
	cs.sendMessage(ChatColor.GOLD + "Warning"+i + " : " + putil.getWarn(i, id));	
	}
	cs.sendMessage(ChatColor.GOLD + "Muted : " + putil.isMuted(id));
	cs.sendMessage(ChatColor.GOLD + "Last ban reason : " + putil.getBanReason(id));
	cs.sendMessage(ChatColor.GOLD + "Banned : " + putil.isBanned(id));
	cs.sendMessage(ChatColor.GOLD + "Tempbanned : " + putil.isTempBanned(id));
	return false;
	}
	}
	return false;	
	}
	if(cs instanceof Player){
	Player p = (Player) cs;	
	if(args.length == 0){
	UUID id = putil.getPlayer(p.getName());
	p.sendMessage(ChatColor.GOLD + p.getName() + " Warnings / Bans");
	for(int i = 1; i<6; i++){
	cs.sendMessage(ChatColor.GOLD + "Warning"+i + " : " + putil.getWarn(i, id));	
	}
	cs.sendMessage(ChatColor.GOLD + "Muted : " + putil.isMuted(id));
	if(putil.isBanned(id)){
	p.sendMessage(ChatColor.GOLD + "Last ban reason : " + putil.getBanReason(id));
	if(putil.isTempBanned(id)){
	p.sendMessage(ChatColor.GOLD + "Tempbanned : " + putil.isTempBanned(id));	
	}
	
	}
	p.sendMessage(ChatColor.GOLD + "Banned : " + putil.isBanned(id));
	return false;
	}
	if(args.length == 1){
	String name = putil.getName(args[0]);
	UUID id = putil.getPlayer(name);	
	if(id == null){
	p.sendMessage(ChatColor.RED + args[0] + " isn't in our files");
	return false;	
	}
	if(id !=null){
	p.sendMessage(ChatColor.GOLD + args[0] + " Warnings / Bans");
	cs.sendMessage(ChatColor.GOLD + "Muted : " + putil.isMuted(id));
	for(int i = 1; i<6; i++){
	cs.sendMessage(ChatColor.GOLD + "Warning"+i + " : " + putil.getWarn(i, id));	
	}
	p.sendMessage(ChatColor.GOLD + "Banned : " + putil.isBanned(id));
	if(putil.isBanned(id)){
	p.sendMessage(ChatColor.GOLD + "Last ban reason : " + putil.getBanReason(id));
	if(putil.isTempBanned(id)){
	p.sendMessage(ChatColor.GOLD + "Tempbanned : " + putil.isTempBanned(id));	
	}
	}
	return false;
	}
	}
	}
		
	}
	return false;
	}

}
