package net.novagaming.maxbans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ban implements CommandExecutor{
	
	  String now(){
	  DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	  Date date = new Date();
	  return dateFormat.format(date);
	  }

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String lab,String[] args) {
    if(cmd.getName().equalsIgnoreCase("ban")){
    if(!cs.hasPermission(permissions.ban)){
    cs.sendMessage(ChatColor.RED + "You don't have permission for this command if you believe this is an error contact the server admins");
    return false;	
    }
    if(cs.hasPermission(permissions.ban)){
    if(args.length < 2){
    cs.sendMessage(ChatColor.RED + "/ban <name> <reason>");
    return false;	
    }
    UUID id  = putil.getPlayer(args[0]);
    StringBuilder str = new StringBuilder();
    for(int i = 1; i<args.length; i++){
    str.append(args[i] + " ");	
    }
    Player target = (Player) Bukkit.getServer().getPlayer(args[0]);
    if(target !=null){
    target.kickPlayer(ChatColor.GOLD + cs.getName() + " Banned you for : " + str.toString());	
    }
    Bukkit.broadcastMessage(ChatColor.GOLD + args[0] + " was banned for " + str.toString() + " by : " + cs.getName());
    putil.addBan(id,str.toString(),cs.getName());
    }
    }
	return false;
	}

}
