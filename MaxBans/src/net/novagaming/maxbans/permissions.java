package net.novagaming.maxbans;

import org.bukkit.permissions.Permission;
import org.bukkit.plugin.PluginManager;

public class permissions {
	
	public static Permission ban = new Permission("maxbans.ban");
	public static Permission banip = new Permission("maxbans.banip");
	public static Permission banlist = new Permission("maxbans.banlist");
	public static Permission dupeip = new Permission("maxbans.dupeip");
	public static Permission kick = new Permission("maxbans.kick");
	public static Permission mute = new Permission("maxbans.mute");
	public static Permission tempban = new Permission("maxbans.tempban");
	public static Permission rangebanip = new Permission("maxbans.rangebanip");
	public static Permission unban = new Permission("maxbans.unban");
	public static Permission unmute = new Permission("maxbans.unmute");
	public static Permission unrangebanip = new Permission("maxbans.unrangebanip");
	public static Permission warn = new Permission("maxbans.warn");

	
	public static void registerPerms(PluginManager pm){
	pm.addPermission(ban);
	pm.addPermission(banip);
	pm.addPermission(banlist);
	pm.addPermission(dupeip);	
	pm.addPermission(kick);
	pm.addPermission(mute);
	pm.addPermission(tempban);
	pm.addPermission(rangebanip);
	pm.addPermission(unban);
	pm.addPermission(unmute);
	pm.addPermission(unrangebanip);
	pm.addPermission(warn);
	}
}
