package maxbans.listeners;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;

import maxbans.playerutil;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class chat implements Listener{

@EventHandler(ignoreCancelled = true,priority=EventPriority.MONITOR)
public void onPreLogin(AsyncPlayerChatEvent event){
UUID id = event.getPlayer().getUniqueId();
if(playerutil.getInstance().isMuted(id)){
long time = playerutil.getInstance().getMuteTime(id);
if(System.currentTimeMillis() > time){
playerutil.getInstance().removeMute(id);
}else{
Calendar c = Calendar.getInstance();
c.setTimeInMillis(time);
String date = now("dd-MM-yyyy hh-mm-ss",c);
event.getPlayer().sendMessage(ChatColor.GOLD + "You have been muted");
event.getPlayer().sendMessage(ChatColor.GOLD + "You will be unmuted on " + date);
event.setCancelled(true);	
}
}

}


public static String now(String dateFormat,Calendar cal) {
SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
return sdf.format(cal.getTime());
}

}
