package maxbans.commands;

import java.util.UUID;

import maxbans.fileutil;
import maxbans.playerutil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ban implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String lab,String[] args) {
    if(cmd.getName().equalsIgnoreCase("ban")){
    if(!cs.hasPermission(permissions.ban)){
    cs.sendMessage(fileutil.getInstance().getConfigMessage("error-nopermission"));
    return false;	
    }
    if(cs.hasPermission(permissions.ban)){
    if(args.length < 2){
    cs.sendMessage(ChatColor.RED + "/ban <name> <reason>");
    return false;	
    }
    String username = args[0];
    UUID id  = playerutil.getInstance().getUUID(username);
    if(id == null){
    cs.sendMessage(fileutil.getInstance().getConfigMessage("error-nodata"));
    return false;	
    }
    StringBuilder str = new StringBuilder();
    for(int i = 1; i<args.length; i++){
    str.append(args[i] + " ");	
    }
    String reason = str.toString();
    String banned = fileutil.getInstance().getConfigMessage("msg-banned");
    banned = banned
    .replace("{USER}",username)
    .replace("{REASON}",reason)
    .replace("{SENDER}",cs.getName());
    Player target = (Player) Bukkit.getServer().getPlayer(username);
    if(target !=null){
    target.kickPlayer(banned);	
    }
    Bukkit.broadcastMessage(banned);
    playerutil.getInstance().addBan(id,ChatColor.RESET +"" + banned);
    }
    }
	return false;
	}

}
