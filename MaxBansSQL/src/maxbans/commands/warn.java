package maxbans.commands;

import java.util.UUID;

import maxbans.fileutil;
import maxbans.playerutil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class warn implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String lab,String[] args) {
    if(cmd.getName().equalsIgnoreCase("warn")){
    if(!cs.hasPermission(permissions.warn)){
    cs.sendMessage(fileutil.getInstance().getConfigMessage("error-nopermission"));
    return false;	
    }
    if(cs.hasPermission(permissions.warn)){
    if(args.length < 2){
    cs.sendMessage(ChatColor.RED + "/warn <name> <reason>");
    return false;	
    }
    String username = args[0];
    UUID id  = playerutil.getInstance().getUUID(username);
    StringBuilder str = new StringBuilder();
    for(int i = 1; i<args.length; i++){
    str.append(args[i] + " ");	
    }
    String reason = str.toString();
    playerutil.getInstance().addWarn(id,reason);
    if(id == null){
    cs.sendMessage(fileutil.getInstance().getConfigMessage("error-nodata"));
    return false;	
    }
    String warnmessage = fileutil.getInstance().getConfigMessage("msg-warned");
    warnmessage = warnmessage
    .replace("{USER}",username)
    .replace("{REASON}",reason)
    .replace("{SENDER}",cs.getName())
    .trim()
    .toString();
    Bukkit.broadcastMessage(warnmessage);
    int warns = playerutil.getInstance().getWarnings(id);
    if(warns == 5){
    playerutil.getInstance().addTempBan(id,str.toString(),System.currentTimeMillis() + 3600 * 1000);	
    playerutil.getInstance().removeWarn(id);
    playerutil.getInstance().removeWarn(id);
    playerutil.getInstance().removeWarn(id);
    playerutil.getInstance().removeWarn(id);
    playerutil.getInstance().removeWarn(id);
    Player p = Bukkit.getServer().getPlayer(username);
    if(p !=null){
    p.kickPlayer(ChatColor.GOLD + "Banned Max Warnings\n Reason"+reason);	
    }
    }
    }
    }
	return false;
	}

}
