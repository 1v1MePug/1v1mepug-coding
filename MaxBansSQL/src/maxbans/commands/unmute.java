package maxbans.commands;

import java.util.UUID;

import maxbans.fileutil;
import maxbans.playerutil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class unmute implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String lab,String[] args) {
    if(cmd.getName().equalsIgnoreCase("unmute")){
    if(!cs.hasPermission(permissions.unmute)){
    cs.sendMessage(ChatColor.RED + "You don't have permission for this command if you believe this is an error contact the server admins");
    return false;	
    }
    if(cs.hasPermission(permissions.unmute)){
    if(args.length <1){
    cs.sendMessage(ChatColor.RED + "/unmute <name>");
    return false;	
    }
    String username = args[0];
    UUID id  = playerutil.getInstance().getUUID(username);
    if(id == null){
    cs.sendMessage(fileutil.getInstance().getConfigMessage("error-nodata"));
    return false;	
    }
    String unmuted = fileutil.getInstance().getConfigMessage("msg-unmuted");
    unmuted = unmuted
    .replace("{USER}",username)
    .replace("{SENDER}",cs.getName());
    Bukkit.broadcastMessage(unmuted);
    playerutil.getInstance().removeMute(id);
    }
    }
	return false;
	}

}
