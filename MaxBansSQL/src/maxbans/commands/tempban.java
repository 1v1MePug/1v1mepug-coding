package maxbans.commands;

import java.util.UUID;

import maxbans.fileutil;
import maxbans.playerutil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class tempban implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String lab,String[] args) {
    if(cmd.getName().equalsIgnoreCase("tempban")){
    if(!cs.hasPermission(permissions.tempban)){
    cs.sendMessage(fileutil.getInstance().getConfigMessage("error-nopermission"));
    return false;	
    }
    if(cs.hasPermission(permissions.tempban)){
    if(args.length < 3){
    cs.sendMessage(ChatColor.RED + "/tempban <name> <time> <timeformat> <reason>");
    return false;	
    }
    String username = args[0];
    UUID id  = playerutil.getInstance().getUUID(args[0]);
    if(id == null){
    cs.sendMessage(fileutil.getInstance().getConfigMessage("error-nodata"));
    return false;	
    }
    int time = Integer.parseInt(args[1]);
    String format = args[2];
    StringBuilder str = new StringBuilder();
    if ((format.equalsIgnoreCase("seconds")) || (format.equalsIgnoreCase("s"))) {
    time += 0;
    }
    if ((format.equalsIgnoreCase("minutes")) || (format.equalsIgnoreCase("min"))) {
    time *= 60;
    }
    if ((format.equalsIgnoreCase("hours")) || (format.equalsIgnoreCase("h"))) {
    time = time * 60 * 60;
    }
    if ((format.equalsIgnoreCase("days")) || (format.equalsIgnoreCase("d"))) {
    time = time * 60 * 60 * 24;
    }
    if ((format.equalsIgnoreCase("weeks")) || (format.equalsIgnoreCase("w"))) {
    time = time * 60 * 60 * 24 * 7;
    }
    if ((format.equalsIgnoreCase("months")) || (format.equalsIgnoreCase("m"))) {
    time = time * 60 * 60 * 24 * 7 * 30;
    }
    if ((format.equalsIgnoreCase("years")) || (format.equalsIgnoreCase("y"))) {
    time = time * 60 * 60 * 24 * 7 * 30 * 12;
    }
    for(int i = 3; i<args.length; i++){
    str.append(args[i] + " ");	
    }
    String reason = str.toString();
    if(reason.isEmpty()){
    str.append("Misconduct");	
    }
    String mutemessage = fileutil.getInstance().getConfigMessage("msg-tempbanned");
    mutemessage = mutemessage
    .replace("{USER}",username)
    .replace("{REASON}",reason)
    .replace("{SENDER}",cs.getName())
    .replace("{TIME}",time + " " +format)
    .trim()
    .toString();
    playerutil.getInstance().addTempMute(id,mutemessage,System.currentTimeMillis() + time*1000);
    Bukkit.broadcastMessage(mutemessage);
    }
    }
	return false;
	}

}
	