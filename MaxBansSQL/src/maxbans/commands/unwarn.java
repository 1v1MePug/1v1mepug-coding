package maxbans.commands;

import java.util.UUID;

import maxbans.fileutil;
import maxbans.playerutil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class unwarn implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String lab,String[] args) {
    if(cmd.getName().equalsIgnoreCase("unwarn")){
    if(!cs.hasPermission(permissions.warn)){
    cs.sendMessage(ChatColor.RED + "You don't have permission for this command if you believe this is an error contact the server admins");
    return false;	
    }
    if(cs.hasPermission(permissions.warn)){
    if(args.length < 1){
    cs.sendMessage(ChatColor.RED + "/unwarn <name>");
    return false;	
    }
    String user = args[0];
    UUID id  = playerutil.getInstance().getUUID(user);
    if(id == null){
    cs.sendMessage(fileutil.getInstance().getConfigMessage("error-nodata"));
    return false;	
    }
    if(id !=null){
    int warnings = playerutil.getInstance().getWarnings(id);
    if(warnings == 0){
    cs.sendMessage(fileutil.getInstance().getConfigMessage("error-nowarningtoremove"));
    return false;	
    }
    if(warnings >=1){
    String removewarn = fileutil.getInstance().getConfigMessage("msg-removedwarning");
    removewarn = removewarn
    .replace("{USER}",user)
    .replace("{SENDER}",cs.getName());
    playerutil.getInstance().removeWarn(id);
    Bukkit.broadcastMessage(removewarn);
    }
    }
    }
    }
	return false;
	}

}
