package maxbans.commands;

import maxbans.fileutil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class kick implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String lab,String[] args) {
    if(cmd.getName().equalsIgnoreCase("kick")){
    if(!cs.hasPermission(permissions.kick)){
    cs.sendMessage(fileutil.getInstance().getConfigMessage("error-nopermission"));
    return false;	
    }
    if(cs.hasPermission(permissions.kick)){
    if(args.length < 2){
    cs.sendMessage(ChatColor.RED + "/kick <name> <reason>");
    return false;	
    }
    String username = args[0];
    StringBuilder str = new StringBuilder();
    for(int i = 1; i<args.length; i++){
    str.append(args[i] + " ");	
    }
    String reason = str.toString();
    String kickmessage = fileutil.getInstance().getConfigMessage("msg-kicked");
    kickmessage = kickmessage
    .replace("{USER}",username)
    .replace("{REASON}",reason)
    .replace("{SENDER}",cs.getName())
    .trim()
    .toString();
    Player p = (Player) Bukkit.getServer().getPlayer(username);
    if(p !=null){
    p.kickPlayer(kickmessage);
    Bukkit.broadcastMessage(kickmessage);
    }else{
    String error = fileutil.getInstance().getConfigMessage("error-playernotfound");
    error = error.replace("{USER}",username);
    cs.sendMessage(error);	
    }
    
    }
    }
	return false;
	}

}
