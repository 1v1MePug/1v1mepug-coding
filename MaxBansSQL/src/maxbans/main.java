package maxbans;

import maxbans.commands.ban;
import maxbans.commands.banlist;
import maxbans.commands.kick;
import maxbans.commands.mute;
import maxbans.commands.tempban;
import maxbans.commands.unban;
import maxbans.commands.unmute;
import maxbans.commands.unwarn;
import maxbans.commands.warn;
import maxbans.listeners.chat;
import maxbans.listeners.join;
import maxbans.listeners.leave;
import maxbans.listeners.prelogin;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class main extends JavaPlugin{
	
	
public final join join = new join(); 
public final leave leave = new leave();
public final prelogin prelogin = new prelogin();
public final chat chat = new chat();
	
public void onEnable(){
fileutil.getInstance().setup(getDataFolder());
sqlutil.getInstance().createConnection(this);
PluginManager pm = Bukkit.getServer().getPluginManager();
pm.registerEvents(this.join,this);
pm.registerEvents(this.leave,this);
pm.registerEvents(this.prelogin,this);
pm.registerEvents(this.chat,this);
getCommand("banlist").setExecutor(new banlist());
getCommand("kick").setExecutor(new kick());
getCommand("mute").setExecutor(new mute());
getCommand("unmute").setExecutor(new unmute());
getCommand("ban").setExecutor(new ban());
getCommand("tempban").setExecutor(new tempban());
getCommand("unban").setExecutor(new unban());
getCommand("warn").setExecutor(new warn());
getCommand("unwarn").setExecutor(new unwarn());
}

}
