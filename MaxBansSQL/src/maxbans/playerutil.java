package maxbans;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import org.bukkit.entity.Player;

public class playerutil {
	
	private static playerutil instance = new playerutil();
	
	public static playerutil getInstance(){
	return instance;	
	}
	
	public boolean isRegistered(UUID id){
	boolean result = false;
	if(this.getInt(5, id) == 1){
	result = true;	
	}else{
	result = false;	
	}
	return result;
	}
	
	public Long getBanTime(UUID id){
	return this.getLong(9,id);	
	}
	public Long getMuteTime(UUID id){
	return this.getLong(10,id);	
	}
	public int getWarnings(UUID id){
	return this.getInt(11, id);	
	}
	
	public String getWarnReason(int i,UUID id){
	String result = null;
	if(i == 1){
	result = this.getString(12, id);	
	}
	if(i == 2){
	result = this.getString(13, id);
	}
	if(i == 3){
	result = this.getString(14, id);	
	}
	if(i == 4){
	result = this.getString(15,id);	
	}
	if(i == 5){
	result = this.getString(16, id);	
	}
	return result;	
	}
	
	public void addWarn(UUID id,String reason){
	try{
	int current = this.getWarnings(id) + 1;
	Statement st = sqlutil.getInstance().c.createStatement();
    String table = fileutil.getInstance().getMYSQLVariable("table");
    st.executeUpdate("UPDATE "+table+" SET Warnings='"+current+"' WHERE UUID='"+id.toString()+ "';");
    st.executeUpdate("UPDATE "+table+" SET Warn"+current+"='"+reason+"' WHERE UUID='"+id.toString()+ "';");
	}catch(Exception e){
	}
	}
	
	public void removeWarn(UUID id){
	try{
	int current = this.getWarnings(id) - 1;
	Statement st = sqlutil.getInstance().c.createStatement();
    String table = fileutil.getInstance().getMYSQLVariable("table");
    st.executeUpdate("UPDATE "+table+" SET Warnings='"+current+"' WHERE UUID='"+id.toString()+ "';");
	}catch(Exception e){
	}
	}
	
	
	
    public void addBan(UUID id,String reason){
    try{
    Statement st = sqlutil.getInstance().c.createStatement();
    int key = 1;
    String table = fileutil.getInstance().getMYSQLVariable("table");
    st.executeUpdate("UPDATE "+table+" SET BanReason='"+reason+"' WHERE UUID='"+id.toString()+ "';");
    st.executeUpdate("UPDATE "+table+" SET Banned='"+key+"' WHERE UUID='"+id.toString()+ "';");
    }catch (Exception e){    	
    }	
    }
    
    public void addTempMute(UUID id,String reason,long bantime){
    try{
    Statement st = sqlutil.getInstance().c.createStatement();
    String table = fileutil.getInstance().getMYSQLVariable("table");
    st.executeUpdate("UPDATE "+table+" SET MuteTime='"+bantime+"' WHERE UUID='"+id.toString()+ "';");
    st.executeUpdate("UPDATE "+table+" SET Muted='"+1+"' WHERE UUID='"+id.toString()+ "';");
    }catch (Exception e){    	
    e.printStackTrace();
    }	
    }
    
    public void addTempBan(UUID id,String reason,long bantime){
    try{
    Statement st = sqlutil.getInstance().c.createStatement();
    int key = 1;
    String table = fileutil.getInstance().getMYSQLVariable("table");
    st.executeUpdate("UPDATE "+table+" SET BanTime='"+bantime+"' WHERE UUID='"+id.toString()+ "';");
    st.executeUpdate("UPDATE "+table+" SET BanReason='"+reason+"' WHERE UUID='"+id.toString()+ "';");
    st.executeUpdate("UPDATE "+table+" SET TempBanned='"+key+"' WHERE UUID='"+id.toString()+ "';");
    }catch (Exception e){    	
    }	
    }
	
	
    public void removeBan(UUID id){
    try{
    Statement st = sqlutil.getInstance().c.createStatement();
    int key = 0;
    String table = fileutil.getInstance().getMYSQLVariable("table");
    st.executeUpdate("UPDATE "+table+" SET BanTime='"+key+"' WHERE UUID='"+id.toString()+ "';");
    st.executeUpdate("UPDATE "+table+" SET Banned='"+key+"' WHERE UUID='"+id.toString()+ "';");
    st.executeUpdate("UPDATE "+table+" SET TempBanned='"+key+"' WHERE UUID='"+id.toString()+ "';");
    }catch (Exception e){    	
    }	
    }
    
   
    public void removeMute(UUID id){
    try{
    Statement st = sqlutil.getInstance().c.createStatement();
    int key = 0;
    String table = fileutil.getInstance().getMYSQLVariable("table");
    st.executeUpdate("UPDATE "+table+" SET MuteTime='"+key+"' WHERE UUID='"+id.toString()+ "';");
    st.executeUpdate("UPDATE "+table+" SET Muted='"+key+"' WHERE UUID='"+id.toString()+ "';");
    }catch (Exception e){    	
    }	
    }
	
	
	public void register(Player p){
	try {
	String table = fileutil.getInstance().getMYSQLVariable("table");
	PreparedStatement ps = sqlutil.getInstance().c.prepareStatement("INSERT INTO `"+table+"` (UUID, Banned, BanReason, Muted, Registered, Username, IP, TempBanned, BanTime, MuteTime, Warnings, Warn1, Warn2, Warn3, Warn4, Warn5) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
	ps.setString(1,p.getUniqueId().toString());
	ps.setInt(2,0);
	ps.setString(3,"");
	ps.setInt(4,0);
	ps.setInt(5,1);
	ps.setString(6,p.getName());
	ps.setString(7,p.getAddress().getAddress().getHostAddress().replaceAll("/",""));
	ps.setInt(8, 0);
	ps.setLong(9,0);
	ps.setLong(10,0);
	ps.setInt(11,0);
	ps.setString(12,"");
	ps.setString(13,"");
	ps.setString(14,"");
	ps.setString(15,"");
	ps.setString(16,"");
	ps.executeUpdate();
	} catch (SQLException e1) {
	e1.printStackTrace();
	}
	}
	
    public void setupTable(String table){
    try{
    Statement statement = sqlutil.getInstance().c.createStatement();	
    statement.execute("CREATE TABLE IF NOT EXISTS `"+table+"` (UUID VARCHAR(36), Banned INT(6), BanReason VARCHAR(110), Muted INT(6), Registered(6), Username VARCHAR(36), IP VARCHAR(36));");
    }catch(Exception e){	
    e.printStackTrace();
    }
    }
   
    
    public UUID getUUID(String username){
    return UUID.fromString(this.getString(1,username));	
    }
    
    public boolean isMuted(UUID id){
    boolean result = false;
    int muted = this.getInt(4, id);
    if(muted == 0){
    result = false;	
    }if(muted == 1){
    result = true;	
    }
    return result;
    }
    
    public boolean isBanned(UUID id){
    boolean result = false;
    int muted = this.getInt(2, id);
    if(muted == 0){
    result = false;	
    }if(muted == 1){
    result = true;	
    }
    return result;
    }
    
    public boolean isTempBanned(UUID id){
    boolean result = false;
    int muted = this.getInt(8, id);
    if(muted == 0){
    result = false;	
    }if(muted == 1){
    result = true;	
    }
    return result;
    }
    
    
    
    public String getBanReason(UUID id){
    return this.getString(3,id);
    }
    
		    
	public void update(Player p){
    try{
    String table = fileutil.getInstance().getMYSQLVariable("table");
	Statement st = sqlutil.getInstance().c.createStatement();
	st.executeUpdate("UPDATE "+table+" SET Username='"+p.getName()+"' WHERE UUID='"+p.getUniqueId().toString() + "';");
	}catch (Exception e){    	
	}
	}
	

	public String getString(int value,UUID id){
	String table = fileutil.getInstance().getMYSQLVariable("table");
	String result = null;
	try {
	Statement statement = sqlutil.getInstance().c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE UUID ='" +id.toString()+ "';");
	res.next();
	result = res.getString(value);
	} catch (SQLException e){
	result = "No data found";
	}
	return result;
	}
	
	public String getString(int value,String id){
	String table = fileutil.getInstance().getMYSQLVariable("table");
	String result = null;
	try {
	Statement statement = sqlutil.getInstance().c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE Username ='" +id.toString()+ "';");
	res.next();
	result = res.getString(value);
	} catch (SQLException e){
	result = "No data found";
	}
	return result;
	}
	
	public int getInt(int value,String id){
	String table = fileutil.getInstance().getMYSQLVariable("table");
	int result = 0;
	try {
	Statement statement = sqlutil.getInstance().c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE Username ='" +id.toString()+ "';");
	res.next();
	result = res.getInt(value);
	} catch (SQLException e){
	}
	return result;
	}
	
	public int getInt(int value,UUID id){
	String table = fileutil.getInstance().getMYSQLVariable("table");
	int result = 0;
	try {
	Statement statement = sqlutil.getInstance().c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE UUID ='" +id.toString()+ "';");
	res.next();
	result = res.getInt(value);
	} catch (SQLException e){
	}
	return result;
	}
	
	
	public long getLong(int value,UUID id){
	String table = fileutil.getInstance().getMYSQLVariable("table");
	long result = 0;
	try {
	Statement statement = sqlutil.getInstance().c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE UUID ='" +id.toString()+ "';");
	res.next();
	result = res.getLong(value);
	} catch (SQLException e){
	}
	return result;
	}
	
	
	public double getDouble(int value,UUID id){
	String table = fileutil.getInstance().getMYSQLVariable("table");
	double result = 0;
	try {
	Statement statement = sqlutil.getInstance().c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE UUID ='" +id.toString()+ "';");
	res.next();
	result = res.getDouble(value);
	} catch (SQLException e){
	}
	return result;
	}

}
