package net.sqldata;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import code.husky.mysql.MySQL;

public class playerutil {
	
	private static playerutil instance = new playerutil();
	MySQL mysql = null;
	Connection c = null;
	
	
	public static playerutil getInstance(){
	return instance;	
	}
	
	public boolean isRegistered(UUID id){
	boolean result = false;
	try{
	Statement st = c.createStatement();
	st.executeQuery("SELECT * FROM players WHERE UUID='"+id.toString()+"';");
	ResultSet rs = st.getResultSet();
	if(rs.getString(1) != null){
	result = true;	
	}	
	}catch(Exception e){
	}
	return result;
	}
	
	
	public void register(Player p){
	try {
	String table = "players";
	PreparedStatement ps = c.prepareStatement("INSERT INTO `"+table+"` (UUID, Username, CrateKeys) VALUES (?, ?, ?);");
	ps.setString(1,p.getUniqueId().toString());
	ps.setString(2,p.getName());
	ps.setInt(3,0);
	ps.executeUpdate();
	} catch (SQLException e1) {
	}
	}
	
    public void setupTable(String table){
    try{
    Statement statement = c.createStatement();	
    statement.execute("CREATE TABLE IF NOT EXISTS `"+table+"` (UUID VARCHAR(36), Username VARCHAR(36), CrateKeys INT(6));");
    }catch(Exception e){	
    }
    }
   
    public int getKeys(String Username){
    return this.getInt(3,Username);	
    }
    
    public void addKeys(String username,int keys){
    try{
    Statement st = c.createStatement();
    String table = "players";
    int key = this.getInt(3,username) + keys;
    st.executeUpdate("UPDATE "+table+" SET CrateKeys='"+key+"' WHERE Username='"+username+ "';");
    }catch (Exception e){    	
    }	
    }
    
    public void removeKeys(String username,int keys){
    try{
    Statement st = c.createStatement();
    String table = "players";
    int key = this.getInt(3,username) - keys;
    st.executeUpdate("UPDATE "+table+" SET CrateKeys='"+key+"' WHERE Username='"+username+ "';");
    }catch (Exception e){    	
    }	
    }
		    
		    
	public void update(Player p){
    try{
    String table = "players";
	Statement st = c.createStatement();
	st.executeUpdate("UPDATE "+table+" SET Username='"+p.getName()+"' WHERE UUID='"+p.getUniqueId().toString() + "';");
	}catch (Exception e){    	
	}
	}
	
	public void createConnection(Plugin p,String ip,String port,String table,String user,String pass){
	mysql = new MySQL(p,ip,port,table,user,pass);
	try {
	c = mysql.openConnection();
	} catch (Exception e) {
	}
	}
	
	public String getString(int value,UUID id){
	String table = "players";
	String result = null;
	try {
	Statement statement = c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE UUID ='" +id.toString()+ "';");
	res.next();
	result = res.getString(value);
	} catch (SQLException e){
	}
	return result;
	}
	
	public int getInt(int value,String id){
	String table = "players";
	int result = 0;
	try {
	Statement statement = c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE Username ='" +id.toString()+ "';");
	res.next();
	result = res.getInt(value);
	} catch (SQLException e){
	}
	return result;
	}
	
	public int getInt(int value,UUID id){
	String table = "players";
	int result = 0;
	try {
	Statement statement = c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE UUID ='" +id.toString()+ "';");
	res.next();
	result = res.getInt(value);
	} catch (SQLException e){
	}
	return result;
	}
	
	public double getDouble(int value,UUID id){
	String table = "players";
	double result = 0;
	try {
	Statement statement = c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE UUID ='" +id.toString()+ "';");
	res.next();
	result = res.getDouble(value);
	} catch (SQLException e){
	}
	return result;
	}

}
