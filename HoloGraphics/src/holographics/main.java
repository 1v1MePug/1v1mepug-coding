package holographics;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import de.inventivegames.hologram.Hologram;
import de.inventivegames.hologram.HologramAPI;
import de.inventivegames.hologram.view.ViewHandler;

public class main extends JavaPlugin{

public void onEnable() {
saveDefaultConfig();
for (String str : getConfig().getKeys(false)) {
ConfigurationSection s = getConfig().getConfigurationSection(str);
ConfigurationSection l = s.getConfigurationSection("loc");
World w = Bukkit.getServer().getWorld(l.getString("world"));
double x = l.getDouble("x"), y = l.getDouble("y"), z = l.getDouble("z");
String message = l.getString("msg").replace('&','�');
Location location = new Location(w,x,y,z);
Hologram hologram = HologramAPI.createHologram(location,message);
hologram.addViewHandler(new ViewHandler() {
@Override
public String onView(Hologram hologram, Player player, String string) {
return string.replace("%%player%%", player.getName());
}
});
hologram.spawn();
}   
}


@Override
public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
Player p = (Player) sender;  
if (cmd.getName().equalsIgnoreCase("createholo")) {
if(p.isOp()){
if(args.length >= 1){
StringBuilder builder = new StringBuilder();
for(int i = 0; i<args.length; i++){
builder.append(args[i] + " ");	
}
String msg = builder.toString();
Location loc = p.getLocation();
double x= loc.getX();
double y = loc.getY();
double z = loc.getZ();
World w = loc.getWorld();
holo holo = new holo(x,y,z,w,msg);
Location location = new Location(w,x,y,z);
save(holo);
Hologram hologram = HologramAPI.createHologram(location,msg);
hologram.addViewHandler(new ViewHandler() {
@Override
public String onView(Hologram hologram, Player player, String string) {
return string.replace("%%player%%", player.getName());
}
});
hologram.spawn();
p.sendMessage(ChatColor.GOLD + "You have made a hologram");
}
}
}       
return true;
}

public void save(holo holo) {
int size = getConfig().getKeys(false).size() + 1;       
getConfig().set(size + ".loc.world", holo.getLocation().getWorld().getName());
getConfig().set(size + ".loc.x", holo.getLocation().getX());
getConfig().set(size + ".loc.y", holo.getLocation().getY());
getConfig().set(size + ".loc.z", holo.getLocation().getZ());
getConfig().set(size + ".loc.msg",holo.getMsg());
saveConfig();
}

}
