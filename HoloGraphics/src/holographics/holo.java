package holographics;

import org.bukkit.Location;
import org.bukkit.World;

public class holo {
	
private double x,y,z;
private World w;

private String msg;

public holo(double x,double y,double z,World w,String msg){
this.setX(x);
this.setY(y);
this.setZ(z);
this.setW(w);
this.setMsg(msg);
}

public Location getLocation(){
Location loc = new Location(w,x,y,z);
return loc;
}

public double getY() {
	return y;
}

public void setY(double y) {
	this.y = y;
}

public World getW() {
	return w;
}

public void setW(World w) {
	this.w = w;
}

public String getMsg() {
	return msg;
}

public void setMsg(String msg) {
	this.msg = msg;
}

public double getX() {
	return x;
}

public void setX(double x) {
	this.x = x;
}

public double getZ() {
	return z;
}

public void setZ(double z) {
	this.z = z;
}

}
