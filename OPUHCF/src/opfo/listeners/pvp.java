package opfo.listeners;

import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class pvp implements Listener{
	
@EventHandler
public void onDeath(PlayerDeathEvent event){
Player p = (Player) event.getEntity();
if(p.getKiller() instanceof Player){
Player p2 = (Player) p.getKiller();
event.setDeathMessage("");
ItemStack is = new ItemStack(Material.SKULL_ITEM,1,(short)SkullType.PLAYER.ordinal());
SkullMeta sm = (SkullMeta) is.getItemMeta();
sm.setDisplayName(p.getName() + "'s head");
sm.setOwner(p.getName());
is.setItemMeta(sm);
World w = p2.getWorld();
w.dropItem(p2.getLocation(),is);
}
}
}
