package opfo.listeners;

import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

public class mobloot implements Listener{
	
	@EventHandler
	public void onEntityDeath(EntityDeathEvent event){
	if(event.getEntity().getKiller() instanceof Player && !(event.getEntity() instanceof Player)){
	event.getDrops().clear();
	Player p = (Player) event.getEntity().getKiller();
	EntityType type = event.getEntityType();
	World w = event.getEntity().getWorld();
	Location loc = event.getEntity().getLocation();
	if(this.getItemPlayer(type,0) !=null){
	w.dropItemNaturally(loc,this.getItemPlayer(type,p.getItemInHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_MOBS)));
	}
	}
	if(!(event.getEntity().getKiller() instanceof Player) && !(event.getEntity() instanceof Player)){
	event.getDrops().clear();
	EntityType type = event.getEntityType();
	World w = event.getEntity().getWorld();
	Location loc = event.getEntity().getLocation();
	if(this.getItem(type) !=null){
	w.dropItemNaturally(loc,this.getItem(type));
	}
	}
	}

	public ItemStack getItemPlayer(EntityType type,int looting){
	ItemStack is = null;
	Random r = new Random();
	int r2 = 0;
	int extra = looting + r.nextInt(5);
	if(type == EntityType.PIG){
	r2 = r.nextInt(5)  + extra;
	is = new ItemStack(Material.PORK,r2);
	}
	if(type == EntityType.VILLAGER){
	r2 = r.nextInt(2)  + extra;
	is = new ItemStack(Material.EMERALD,r2);
	}
	if(type == EntityType.ENDERMAN){
	r2 = r.nextInt(5)  + extra;
	is = new ItemStack(Material.ENDER_PEARL,r2);
	}
	if(type == EntityType.GHAST){
	r2 = r.nextInt(3)  + extra;
	is = new ItemStack(Material.GHAST_TEAR,r2);
	}
	if(type == EntityType.MAGMA_CUBE){
	r2 = r.nextInt(5)  + extra;
	is = new ItemStack(Material.MAGMA_CREAM,r2);
	}
	if(type == EntityType.SLIME){
	r2 = r.nextInt(4)  + extra;
	is = new ItemStack(Material.SLIME_BALL,r2);
	}
	if(type == EntityType.SHEEP){
	r2 = r.nextInt(3)  + extra;
	is = new ItemStack(Material.WOOL,r2);
	}
	if(type == EntityType.CHICKEN){
	r2 = r.nextInt(4)  + extra;
	is = new ItemStack(Material.COOKED_CHICKEN,r2);
	}
	if(type == EntityType.COW){
	r2 = r.nextInt(4)  + extra;
	is = new ItemStack(Material.COOKED_BEEF,r2);
	}
	if(type == EntityType.CREEPER){
	r2 = r.nextInt(7)  + extra;
	is = new ItemStack(Material.SULPHUR,r2);
	}
	if(type == EntityType.ZOMBIE){
	r2 = r.nextInt(8)  + extra;
	is = new ItemStack(Material.ROTTEN_FLESH);
	}
	if(type == EntityType.WITHER){
	r2 = 1;
	is = new ItemStack(Material.NETHER_STAR,r2);
	}
	if(type == EntityType.SKELETON){
	r2 = r.nextInt(8)  + extra;
	is = new ItemStack(Material.BONE,r2);
	}
	if(type == EntityType.IRON_GOLEM){
	r2 = r.nextInt(9)  + extra;
	is = new ItemStack(Material.IRON_INGOT,r2);
	}
	if(type == EntityType.BLAZE){
	r2 = r.nextInt(7)  + extra;
	is = new ItemStack(Material.BLAZE_ROD,r2);	
	}
	if(type == EntityType.SQUID){
	r2 = r.nextInt(5) + extra;
	is = new ItemStack(Material.INK_SACK,r2);
	}
	return is;
	}
		
		
	public ItemStack getItem(EntityType type){
	ItemStack is = null;
	Random r = new Random();
	int r2 = 0;
	if(type == EntityType.PIG){
	r2 = r.nextInt(5);
	is = new ItemStack(Material.PORK,r2);
	}
	if(type == EntityType.VILLAGER){
	r2 = r.nextInt(2);
	is = new ItemStack(Material.EMERALD,3);
	}
	if(type == EntityType.ENDERMAN){
	r2 = r.nextInt(5);
	is = new ItemStack(Material.ENDER_PEARL,r2);
	}
	if(type == EntityType.GHAST){
	r2 = r.nextInt(3);
	is = new ItemStack(Material.GHAST_TEAR,r2);
	}
	if(type == EntityType.MAGMA_CUBE){
	r2 = r.nextInt(5);
	is = new ItemStack(Material.MAGMA_CREAM,r2);
	}
	if(type == EntityType.SLIME){
	r2 = r.nextInt(4);
	is = new ItemStack(Material.SLIME_BALL,r2);
	}
	if(type == EntityType.SHEEP){
	r2 = r.nextInt(3);
	is = new ItemStack(Material.WOOL,r2);
	}
	if(type == EntityType.CHICKEN){
	r2 = r.nextInt(4);
	is = new ItemStack(Material.COOKED_CHICKEN,r2);
	}
	if(type == EntityType.COW){
	r2 = r.nextInt(4);
	is = new ItemStack(Material.COOKED_BEEF,r2);
	}
	if(type == EntityType.CREEPER){
	r2 = r.nextInt(7);
	is = new ItemStack(Material.SULPHUR,r2);
	}
	if(type == EntityType.ZOMBIE){
	r2 = r.nextInt(8);
	is = new ItemStack(Material.ROTTEN_FLESH);
	}
	if(type == EntityType.WITHER){
	r2 = 1;
	is = new ItemStack(Material.NETHER_STAR,r2);
	}
	if(type == EntityType.SKELETON){
	r2 = r.nextInt(8);
	is = new ItemStack(Material.BONE,r2);
	}
	if(type == EntityType.IRON_GOLEM){
	r2 = r.nextInt(9);
	is = new ItemStack(Material.IRON_INGOT,r2);
	}
	if(type == EntityType.BLAZE){
	r2 = r.nextInt(7);
	is = new ItemStack(Material.BLAZE_ROD,r2);	
	}
	if(type == EntityType.SQUID){
	r2 = r.nextInt(5);
	is = new ItemStack(Material.INK_SACK,r2);
	}
	return is;
	}

}
