package opfo;

import opfo.listeners.blockbreak;
import opfo.listeners.mobloot;
import opfo.listeners.pvp;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class main extends JavaPlugin{
	
public final pvp pvp = new pvp();
public final blockbreak bb = new blockbreak();
public final mobloot mb = new mobloot();
	
public void onEnable(){
PluginManager pm = Bukkit.getServer().getPluginManager();
pm.registerEvents(this.pvp,this);
pm.registerEvents(this.bb,this);
pm.registerEvents(this.mb,this);
}

}
