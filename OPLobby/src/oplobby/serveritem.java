package oplobby;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class serveritem {
	
private String servername;
private String serverip;
private int serverport;

private Material material;
private int itemslot;

private boolean load;

public serveritem(String servername,String serverip,int serverport,Material material,int itemslot,boolean load){
this.servername = servername;
this.serverip = serverip;
this.serverport = serverport;
this.material = material;
this.itemslot = itemslot;
this.load = load;
}

public boolean isLoaded(){
return this.load;	
}

public String getServerName(){
return this.servername;	
}

public String getServerIP(){
return this.serverip;	
}

public int getServerPort(){
return this.serverport;	
}

public Material getMaterial(){
return this.material;	
}

public int getItemSlot(){
return this.itemslot;	
}

public ItemStack getItemStack(){
ItemStack is = new ItemStack(this.getMaterial(),1);
ItemMeta im = is.getItemMeta();
im.setDisplayName(getServerName());
ArrayList<String> lore = new ArrayList<String>();
lore.add("Players : " + this.getServerOnlinePlayers() + " / " + this.getServerMaxPlayers() + " ("+this.getPercentageOnline()+"%)");
im.setLore(lore);
is.setItemMeta(im);
return is;
}

public int getServerOnlinePlayers(){
int count = 0;
try {
Socket sock = new Socket(this.serverip,this.serverport);
DataOutputStream out = new DataOutputStream(sock.getOutputStream());
DataInputStream in = new DataInputStream(sock.getInputStream());		 
out.write(0xFE);		 
int b;
StringBuffer str = new StringBuffer();
while ((b = in.read()) != -1) {
if (b != 0 && b > 16 && b != 255 && b != 23 && b != 24) {
str.append((char) b);
}
}		 
String[] data = str.toString().split("�");
int onlinePlayers = Integer.parseInt(data[1]);
count = onlinePlayers;
sock.close();
} catch (Exception e) {
count = 0;
}
return count;
}

public int getServerMaxPlayers(){
int count = 0;
try {
Socket sock = new Socket(this.serverip,this.serverport);
DataOutputStream out = new DataOutputStream(sock.getOutputStream());
DataInputStream in = new DataInputStream(sock.getInputStream());		 
out.write(0xFE);		 
int b;
StringBuffer str = new StringBuffer();
while ((b = in.read()) != -1) {
if (b != 0 && b > 16 && b != 255 && b != 23 && b != 24) {
str.append((char) b);
}
}		 
String[] data = str.toString().split("�");
int onlinePlayers = Integer.parseInt(data[2]);
count = onlinePlayers;
sock.close();
} catch (Exception e) {
}
return count;
}

public Float getPercentageOnline(){
float count = 0;
if(this.getServerOnlinePlayers() == 0){
count = 0;	
}else{
count = this.getServerOnlinePlayers() * 100 / this.getServerMaxPlayers();
}
return count;
}

}
