package oplobby;

import java.util.ArrayList;

import oplobby.listeners.invclick;
import oplobby.listeners.join;
import oplobby.listeners.leave;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class main extends JavaPlugin{

public static ArrayList<serveritem> servers = new ArrayList<serveritem>();
	
	
public final join join = new join(); 
public final leave leave = new leave();
public final invclick invclick = new invclick();
	
public void onEnable(){
fileutil.getInstance().setup(getDataFolder());
sqlutil.getInstance().createConnection(this);
PluginManager pm = Bukkit.getServer().getPluginManager();
pm.registerEvents(this.join,this);
pm.registerEvents(this.leave,this);
pm.registerEvents(this.invclick,this);
Bukkit.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
}

public static Plugin getPlugin(){
return Bukkit.getPluginManager().getPlugin("OPLobby");	
}



}
