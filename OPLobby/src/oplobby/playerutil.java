package oplobby;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import org.bukkit.entity.Player;

public class playerutil {
	
	private static playerutil instance = new playerutil();
	
	public static playerutil getInstance(){
	return instance;	
	}
	
	public boolean isRegistered(UUID id){
	boolean result = false;
	if(this.getInt(6, id) == 1){
	result = true;	
	}else{
	result = false;	
	}
	return result;
	}
	
	
	public void register(Player p){
	try {
	String table = fileutil.getInstance().getMYSQLVariable("table");
	PreparedStatement ps = sqlutil.getInstance().c.prepareStatement("INSERT INTO `"+table+"` (UUID, Username, CrateKeys, Kills, Deaths, Registered) VALUES (?, ?, ?, ?, ?, ?);");
	ps.setString(1,p.getUniqueId().toString());
	ps.setString(2,p.getName());
	ps.setInt(3,3);
	ps.setInt(4,0);
	ps.setInt(5,0);
	ps.setInt(6,1);
	ps.executeUpdate();
	} catch (SQLException e1) {
	}
	}
	
    public void setupTable(String table){
    try{
    Statement statement = sqlutil.getInstance().c.createStatement();	
    statement.execute("CREATE TABLE IF NOT EXISTS `"+table+"` (UUID VARCHAR(36), Username VARCHAR(36), CrateKeys INT(6), Kills INT(6), Deaths INT(6), Registered(6));");
    }catch(Exception e){	
    }
    }
		    
		    
	public void update(Player p){
    try{
    String table = fileutil.getInstance().getMYSQLVariable("table");
	Statement st = sqlutil.getInstance().c.createStatement();
	st.executeUpdate("UPDATE "+table+" SET Username='"+p.getName()+"' WHERE UUID='"+p.getUniqueId().toString() + "';");
	}catch (Exception e){    	
	}
	}
	

	public String getString(int value,UUID id){
	String table = fileutil.getInstance().getMYSQLVariable("table");
	String result = null;
	try {
	Statement statement = sqlutil.getInstance().c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE UUID ='" +id.toString()+ "';");
	res.next();
	result = res.getString(value);
	} catch (SQLException e){
	}
	return result;
	}
	
	public String getString(int value,String id){
	String table = fileutil.getInstance().getMYSQLVariable("table");
	String result = null;
	try {
	Statement statement = sqlutil.getInstance().c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE Username ='" +id.toString()+ "';");
	res.next();
	result = res.getString(value);
	} catch (SQLException e){
	}
	return result;
	}
	
	public int getInt(int value,String id){
	String table = fileutil.getInstance().getMYSQLVariable("table");
	int result = 0;
	try {
	Statement statement = sqlutil.getInstance().c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE Username ='" +id.toString()+ "';");
	res.next();
	result = res.getInt(value);
	} catch (SQLException e){
	}
	return result;
	}
	
	public int getInt(int value,UUID id){
	String table = fileutil.getInstance().getMYSQLVariable("table");
	int result = 0;
	try {
	Statement statement = sqlutil.getInstance().c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE UUID ='" +id.toString()+ "';");
	res.next();
	result = res.getInt(value);
	} catch (SQLException e){
	}
	return result;
	}
	
	public double getDouble(int value,UUID id){
	String table = fileutil.getInstance().getMYSQLVariable("table");
	double result = 0;
	try {
	Statement statement = sqlutil.getInstance().c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE UUID ='" +id.toString()+ "';");
	res.next();
	result = res.getDouble(value);
	} catch (SQLException e){
	}
	return result;
	}

}
