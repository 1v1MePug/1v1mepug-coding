package oplobby;

import java.io.File;
import java.io.IOException;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class fileutil {
	
private static fileutil instance = new fileutil();

public static fileutil getInstance(){
return instance;	
}

public File conf;
public File servers;

public String getMYSQLVariable(String var){
FileConfiguration config = YamlConfiguration.loadConfiguration(conf);
return config.getString("mysql-"+var);
}

public void save(serveritem serveritem) {
FileConfiguration config = YamlConfiguration.loadConfiguration(servers);
int size = config.getKeys(false).size() + 1;       
config.set(size + ".servers.servername",serveritem.getServerName());
config.set(size + ".servers.serverip",serveritem.getServerIP());
config.set(size + ".servers.serverport",serveritem.getServerPort());
config.set(size + ".servers.material",serveritem.getMaterial().getId());
config.set(size + ".servers.materialitemslot",serveritem.getItemSlot());
config.set(size + ".servers.load",serveritem.isLoaded());
try {
config.save(servers);
} catch (IOException e) {
}
}

public void load(){
FileConfiguration config = YamlConfiguration.loadConfiguration(servers);
for (String str : config.getKeys(false)) {
ConfigurationSection s = config.getConfigurationSection(str);
ConfigurationSection l = s.getConfigurationSection("servers");
String name = l.getString("servername");
String serverip = l.getString("serverip");
int port = l.getInt("serverport");
Material material = Material.getMaterial(l.getInt("material"));
int itemslot = l.getInt("materialitemslot");
boolean load = l.getBoolean("load");
serveritem serveritem = new serveritem(name,serverip,port,material,itemslot,load);
if(load == true){
main.servers.add(serveritem);
}
}   	
}

public void setup(File dir){
if(!dir.exists()){
dir.mkdirs();	
}
servers = new File(dir + File.separator + "servers.yml");
if(!servers.exists()){
try {
servers.createNewFile();
serveritem serveritem = new serveritem("HUB","mc.novagaming.co.uk",25564,Material.BOOK,4,true);
this.save(serveritem);
serveritem serveritem2 = new serveritem("UHC","mc.novagaming.co.uk",25561,Material.OBSIDIAN,9,true);
this.save(serveritem2);
serveritem serveritem3 = new serveritem("KITS","mc.novagaming.co.uk",25563,Material.MUSHROOM_SOUP,17,true);
this.save(serveritem3);
serveritem serveritem4 = new serveritem("POTS","mc.novagaming.co.uk",25562,Material.DIAMOND_SWORD,22,true);
this.save(serveritem4);
} catch (IOException e) {
}	
}
this.load();
conf = new File(dir + File.separator + "conf.yml");
if(!conf.exists()){
try{
FileConfiguration config = YamlConfiguration.loadConfiguration(conf);
config.set("mysql-ip","5.196.128.181");
config.set("mysql-port","3306");
config.set("mysql-user","root");
config.set("mysql-pass","supercode12mam");
config.set("mysql-database","PlayerData");
config.set("mysql-table","players");
config.save(conf);
}catch(Exception e){
}
}

}

}
