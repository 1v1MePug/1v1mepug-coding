package oplobby.listeners;

import oplobby.playerutil;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class join implements Listener {

@EventHandler
public void onJoin(PlayerJoinEvent event){
Player p = (Player) event.getPlayer();
if(!playerutil.getInstance().isRegistered(p.getUniqueId())){
playerutil.getInstance().register(p);
}
if(!p.getInventory().contains(this.getItem())){
p.getInventory().addItem(this.getItem());	
}
event.setJoinMessage("");
}

@EventHandler
public void onRespawn(PlayerRespawnEvent event){
Player p = (Player) event.getPlayer();
p.getInventory().addItem(this.getItem());
}

public ItemStack getItem(){
ItemStack is = new ItemStack(Material.EMERALD,1);
ItemMeta im = is.getItemMeta();
im.setDisplayName("ServerTeleporter");
is.setItemMeta(im);
return is;
}

}
