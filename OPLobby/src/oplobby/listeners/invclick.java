package oplobby.listeners;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import oplobby.main;
import oplobby.serveritem;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class invclick implements Listener{
	
public ArrayList<UUID> players = new ArrayList<UUID>();
	
public void invtimer(final Player p){
if(players.contains(p.getUniqueId())){
final Inventory inv = Bukkit.createInventory(null,27,ChatColor.GOLD + "NovaServerTeleporter");
p.openInventory(inv);
Bukkit.getScheduler().runTaskTimer(main.getPlugin(),new Runnable(){
@Override
public void run() {
inv.clear();
for(serveritem servers : main.servers){
inv.setItem(servers.getItemSlot(),servers.getItemStack());
}
Random r = new Random();
for(int i = 0; i<inv.getSize(); i++){
if(inv.getItem(i) == null){
int r2 = r.nextInt(15);
ItemStack is = new ItemStack(Material.STAINED_GLASS,1,(short)r2);
inv.setItem(i,is);
}
}


}	
},0,60);
}	
}

@EventHandler
public void onInvClose(InventoryCloseEvent event){
Player p = (Player) event.getPlayer();
Inventory inv = event.getInventory();
if(inv.getTitle().equalsIgnoreCase(ChatColor.GOLD+"NovaServerTeleporter")){
players.remove(p.getUniqueId());
}	
}

@EventHandler
public void onInvClick(InventoryClickEvent event){
Player e = (Player) event.getWhoClicked();
Inventory inv = event.getInventory();
if(inv.getTitle().equalsIgnoreCase(ChatColor.GOLD+"NovaServerTeleporter")){
event.setCancelled(true);
}
ItemStack is = event.getCurrentItem();
if(is.hasItemMeta()){
String itemname = is.getItemMeta().getDisplayName();
for(serveritem si : main.servers){
if(si.getServerName().equalsIgnoreCase(itemname)){
ByteArrayOutputStream b = new ByteArrayOutputStream();
DataOutputStream out = new DataOutputStream(b);
try {
out.writeUTF("Connect");
out.writeUTF(si.getServerName());
} catch (IOException e1) {
e1.printStackTrace();
}   
e.getPlayer().sendPluginMessage(main.getPlugin(), "BungeeCord", b.toByteArray());	
}
}
}
}
	
	
@EventHandler()
public void onPlayerInteract(PlayerInteractEvent event){
Player p = (Player) event.getPlayer();
ItemStack is = event.getItem();
if(event.getAction() == Action.RIGHT_CLICK_AIR){
if(is.getType() == Material.EMERALD){
if(is.hasItemMeta()){
String itemname = is.getItemMeta().getDisplayName();
if(itemname.equalsIgnoreCase("ServerTeleporter")){
this.invtimer(p);	
this.players.add(p.getUniqueId());
}
}
}
}
}

}
