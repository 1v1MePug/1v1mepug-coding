package ubu.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;

import ubu.fileutil;

public class near implements CommandExecutor {
	
	public void sendNearbyPlayers(Player p,double radius){
	StringBuilder builder = new StringBuilder();
	for(Entity e : p.getNearbyEntities(radius, radius, radius)){
	if(e instanceof Player){
	Player p2 = (Player) e;
	if(p !=p2){
	int distance = (int)p2.getLocation().distance(p.getLocation());
	builder.append(p2.getDisplayName() + " ("+distance+")" + " , ");
	}
	}
	}
	String prefix = fileutil.getInstance().getMessage("prefix");
	if(!builder.toString().isEmpty()){ 
	String cmdmessage = fileutil.getInstance().getMessage("command-near-msg-playersfound");
	String players = builder.toString();
	p.sendMessage(prefix + cmdmessage + players);
	return;
	}
	if(builder.toString().isEmpty()){
	String cmdmessage = fileutil.getInstance().getMessage("command-near-msg-playersnotfound");
	p.sendMessage(prefix + cmdmessage);	
	return;
	} 

	
	}

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label,String[] args) {
	if(cmd.getName().equalsIgnoreCase("near")){
	if(!(cs instanceof Player)){
	return false;	
	}
	if(cs instanceof Player){
	Player p = (Player) cs;
	Permission perm = fileutil.getInstance().getCommandPermission("near");
	if(!p.hasPermission(perm)){
	p.sendMessage(fileutil.getInstance().getPermissionDeniedMSG());
	return false;
	}
	Permission allranges = new Permission(fileutil.getInstance().getMessage("command-near-allranges-permission"));
	if(p.hasPermission(allranges)){
	if(args.length == 0){
	this.sendNearbyPlayers(p,fileutil.getInstance().getCommandInt("near","radius"));
	return false;	
	}
	if(args.length == 1){
	int parse = Integer.parseInt(args[0]);
	this.sendNearbyPlayers(p,parse);
	return false;
	}
	}
	if(p.hasPermission(perm)){
	this.sendNearbyPlayers(p,fileutil.getInstance().getCommandInt("near","radius"));
	return false;
	}
	}
	}
	return false;
	}

}
