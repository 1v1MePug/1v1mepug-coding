package ubu.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ubu.fileutil;
import ubu.main;

public class donators implements CommandExecutor {
	
public void sendDonators(CommandSender cs){
StringBuilder prince = new StringBuilder();
StringBuilder king = new StringBuilder();
StringBuilder legend = new StringBuilder();
StringBuilder crafter = new StringBuilder();
StringBuilder hero = new StringBuilder();
for(Player p : Bukkit.getServer().getOnlinePlayers()){
String rank = main.permission.getPrimaryGroup(p);
if(rank.equalsIgnoreCase("Hero")){
hero.append(p.getDisplayName() + " , ");	
}
if(rank.equalsIgnoreCase("Crafter")){
crafter.append(p.getDisplayName() + " , ");	
}
if(rank.equalsIgnoreCase("Legend")){
legend.append(p.getDisplayName()  + " , ");	
}
if(rank.equalsIgnoreCase("king")){
king.append(p.getDisplayName() +    " , ");	
}
if(rank.equalsIgnoreCase("prince")){
prince.append(p.getDisplayName() + " , ");	
}
}
String princemsg = prince.toString();
String kingmsg = king.toString();
String legendmsg = legend.toString();
String craftermsg = crafter.toString();
String heromsg = hero.toString();
if(!princemsg.isEmpty()){
String pprefix = fileutil.getInstance().getMessage("prince-prefix");
cs.sendMessage(pprefix + " : " + princemsg);	
}
if(!kingmsg.isEmpty()){
String pprefix = fileutil.getInstance().getMessage("king-prefix");
cs.sendMessage(pprefix + " : " +kingmsg);	
}
if(!legendmsg.isEmpty()){
String pprefix = fileutil.getInstance().getMessage("legend-prefix");
cs.sendMessage(pprefix + " : "  +legendmsg);	
}
if(!craftermsg.isEmpty()){
String pprefix = fileutil.getInstance().getMessage("crafter-prefix");
cs.sendMessage(pprefix + " : " + craftermsg);	
}
if(!heromsg.isEmpty()){
String pprefix = fileutil.getInstance().getMessage("hero-prefix");
cs.sendMessage(pprefix + " : "  + heromsg);	
}
if(princemsg.isEmpty() && kingmsg.isEmpty() && legendmsg.isEmpty() && craftermsg.isEmpty() && heromsg.isEmpty()){
String prefix = fileutil.getInstance().getMessage("prefix");
String message = fileutil.getInstance().getMessage("error-no-donators-online");
cs.sendMessage(prefix + message);
return;	
}
	
	
}

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String arg2,String[] arg3) {
	if(cmd.getName().equalsIgnoreCase("donators")){
	this.sendDonators(cs);	
	}
	return false;
	}

}
