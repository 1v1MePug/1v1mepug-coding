package ubu;

import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import ubu.commands.donators;
import ubu.commands.near;
import ubu.listeners.heads;

public class main extends JavaPlugin {

public final heads heads = new heads();

public static Permission permission;
	
public void onEnable(){
fileutil.getInstance().setup(getDataFolder());
PluginManager pm = Bukkit.getServer().getPluginManager();
pm.registerEvents(this.heads,this);
getCommand("near").setExecutor(new near());
getCommand("donators").setExecutor(new donators());
if(!setupPermissions()){
System.out.println("Plugin disabled due to no permission system found");
this.setEnabled(false);	
}
}

private boolean setupPermissions(){
RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
if (permissionProvider != null) {
permission = permissionProvider.getProvider();
}
return (permission != null);
}


}
