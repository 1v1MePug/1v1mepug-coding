package ubu;

import java.io.File;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.permissions.Permission;

public class fileutil {
	
private static fileutil instance = new fileutil();

public static fileutil getInstance(){
return instance;	
}

public File config;

public String getPermissionDeniedMSG(){
FileConfiguration conf = YamlConfiguration.loadConfiguration(config);		
return conf.getString("error-nopermission").replace('&','�');
}

public String getMessage(String value){
FileConfiguration conf = YamlConfiguration.loadConfiguration(config);
return conf.getString(value).replace('&','�');
}

public Permission getCommandPermission(String cmd){
FileConfiguration conf = YamlConfiguration.loadConfiguration(config);	
Permission perm = new Permission(conf.getString("command-")+cmd+"-permission");
return perm;
}

public int getCommandInt(String command,String value){
FileConfiguration conf = YamlConfiguration.loadConfiguration(config);	
return conf.getInt("command-"+command+"-"+value);
}

public void setup(File dir){
try{
if(!dir.exists()){
dir.mkdirs();	
}
config = new File(dir.getPath() + File.separator + "conf.yml");
if(!config.exists()){
FileConfiguration conf= YamlConfiguration.loadConfiguration(config);
conf.set("prefix","&4[&eThe&cLegend&3Craft&4]&e ");
conf.set("command-near-msg-playersfound","&6Players Nearby : ");
conf.set("command-near-msg-playersnotfound","&cNo players nearby");
conf.set("command-near-range",500);
conf.set("command-near-permission","tlc.near");
conf.set("command-near-allranges-permission","tlc.near.infiniterange");
conf.set("hero-prefix","&6Hero ");
conf.set("crafter-prefix","&6Crafter ");
conf.set("legend-prefix","&6Legend ");
conf.set("king-prefix","&6King ");
conf.set("prince-prefix","&6Prince ");
conf.set("error-nopermission","&cYou don't have permission");
conf.set("error-no-donators-online","&6No donators online");
conf.save(config);
}
}catch (Exception e){
e.printStackTrace();
}
}

}
