package ubu.listeners;


import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class heads implements Listener {
	
@EventHandler
public void onBehead(PlayerDeathEvent event){
if(event.getEntity() instanceof Player){
Player p = (Player) event.getEntity();
String username = p.getName();
Location loc = p.getLocation();
World w = p.getWorld();
ItemStack is = new ItemStack(Material.SKULL_ITEM,1,(short)SkullType.PLAYER.ordinal());
SkullMeta sm = (SkullMeta) is.getItemMeta();
sm.setOwner(username);
sm.setDisplayName(username+"'s head");
is.setItemMeta(sm);
w.dropItemNaturally(loc,is);
}
}

}
