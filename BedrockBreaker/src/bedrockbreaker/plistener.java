package bedrockbreaker;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;

public class plistener implements Listener{
	
public static ArrayList<UUID> bedrockbreaker = new ArrayList<UUID>();

public static boolean hasBedrockBreaker(UUID id){
return bedrockbreaker.contains(id);	
}

public static boolean canBreakBlock(Block b ,Player p){
boolean result = false;
BlockBreakEvent be = new BlockBreakEvent(b,p);
PluginManager pm = Bukkit.getServer().getPluginManager();
pm.callEvent(be);
if(be.isCancelled()){
result = false;	
}else{
World w = b.getWorld();
w.dropItem(b.getLocation(),new ItemStack(Material.BEDROCK));
b.setType(Material.AIR);
b.getState().update();
main.econ.withdrawPlayer(p,main.getInstance().getConfig().getInt("charge-per-bedrock"));
result = true;	
}
return result;
}

@EventHandler()
public void onInteract(PlayerInteractEvent event){
Player p = (Player) event.getPlayer();
ItemStack inhand = p.getItemInHand();
if(event.getAction() == Action.LEFT_CLICK_BLOCK){
if(inhand.getType() == Material.DIAMOND_PICKAXE){
Block b = (Block) event.getClickedBlock();
if(b.getType() == Material.BEDROCK){
UUID id = p.getUniqueId();
if(hasBedrockBreaker(id)){
World w = p.getWorld();
Location loc = p.getLocation();
ItemStack is = new ItemStack(Material.BEDROCK,1);
w.dropItemNaturally(loc,is);
BlockBreakEvent bevent = new BlockBreakEvent(b,p);
Bukkit.getServer().getPluginManager().callEvent(bevent);
if(!canBreakBlock(b,p)){
return;	
}
}
}
}
}
}

}
