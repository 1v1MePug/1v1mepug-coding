package bedrockbreaker;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class main extends JavaPlugin{
	
public final plistener pl = new plistener();
public static Economy econ;
	
	
public void onEnable(){
if(!this.getDataFolder().exists()){
this.getDataFolder().mkdirs();
this.getConfig().set("bb-permission","tlc.bedrockbreaker");
this.getConfig().set("bb-enable-msg","You have enabled bedrockbreaker");
this.getConfig().set("bb-disable-msg","You have disabled bedrockbreaker");
this.getConfig().set("charge-per-bedrock",125);
this.getConfig().set("Author","1V1MePug ");
this.saveConfig();
}
if(!setupEconomy()){
this.setEnabled(false);	
}
PluginManager pm = Bukkit.getServer().getPluginManager();
pm.registerEvents(this.pl,this);
getCommand("bedrockbreaker").setExecutor(new bb());
}

private boolean setupEconomy() {
if (getServer().getPluginManager().getPlugin("Vault") == null) {
return false;
}
RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
if(rsp == null) {
return false;
}
econ = rsp.getProvider();
return econ != null;
}

public static Plugin getInstance(){
return Bukkit.getServer().getPluginManager().getPlugin("BedrockBreaker");
}


}
