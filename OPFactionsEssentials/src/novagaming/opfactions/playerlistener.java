package novagaming.opfactions;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class playerlistener implements Listener{
	
@EventHandler(priority=EventPriority.MONITOR)
public void onJoin(PlayerJoinEvent event){
Player p = (Player) event.getPlayer();
boolean registered = putil.getInstance().registered(p.getUniqueId());
if(!registered){
putil.getInstance().createAccount(p);
}
}

@EventHandler
public void onDeath(PlayerDeathEvent event){
if(event.getEntity().getKiller() instanceof Player){
Player p1 = (Player) event.getEntity();
Player p2 = (Player) event.getEntity().getKiller();
putil.getInstance().addDeath(p1.getUniqueId(),1);
putil.getInstance().addKill(p2.getUniqueId(),1);
}
}

@EventHandler(priority=EventPriority.MONITOR)
public void onQuit(PlayerQuitEvent event){
Player p = (Player) event.getPlayer();
putil.getInstance().updateAccount(p);
}
		
@SuppressWarnings("deprecation")
@EventHandler(ignoreCancelled = false,priority=EventPriority.MONITOR)
public void onItemConsume(PlayerItemConsumeEvent event){
if((event.getItem().getType() == Material.GOLDEN_APPLE) && (event.getItem().getData().getData() == 1)) {
Player p = event.getPlayer();
event.setCancelled(true);
p.updateInventory();
int as = event.getItem().getAmount() - 1;
ItemStack apple = new ItemStack(Material.GOLDEN_APPLE, as,(short)1);
int dura = event.getItem().getDurability();
apple.setDurability((short)dura);
p.getInventory().setItemInHand(apple);
p.updateInventory();
PotionEffect absorp = new PotionEffect(PotionEffectType.ABSORPTION,30 * 20,2);
PotionEffect regen = new PotionEffect(PotionEffectType.REGENERATION,65 * 20,6);
PotionEffect fire = new PotionEffect(PotionEffectType.FIRE_RESISTANCE,65 * 20,6);
PotionEffect strenght = new PotionEffect(PotionEffectType.INCREASE_DAMAGE,30 * 20,0);
p.addPotionEffect(absorp);
p.addPotionEffect(regen);
p.addPotionEffect(fire);
p.addPotionEffect(strenght);
p.setFoodLevel(20);
}	
}

@EventHandler(ignoreCancelled = true,priority=EventPriority.MONITOR)
public void onEntityDamage(EntityDamageByEntityEvent event){
if(event.getEntity() instanceof Player && event.getDamager() instanceof Player){
Player p1 = (Player) event.getEntity();
Player p2 = (Player) event.getDamager();
ItemStack h = p1.getInventory().getHelmet();
ItemStack c = p1.getInventory().getChestplate();
ItemStack l = p1.getInventory().getLeggings();
ItemStack b = p1.getInventory().getBoots();
int sharpness = p2.getItemInHand().getEnchantmentLevel(Enchantment.DAMAGE_ALL);
if(sharpness == 20){
event.setDamage(1.0);	
}
if(sharpness == 22 || sharpness == 23){
event.setDamage(2.0);	
}
if(sharpness == 24 || sharpness == 25){
event.setDamage(2.5);	
}
if(sharpness == 26 || sharpness == 27 ){
event.setDamage(3.0);	
}
if(sharpness == 28 || sharpness == 29){
event.setDamage(4.0);	
}
if(sharpness == 30 || sharpness == 31){
event.setDamage(4.5);	
}
if(sharpness == 32 || sharpness == 33){
event.setDamage(5.0);	
}
if(sharpness == 34 || sharpness == 35){
event.setDamage(6.0);	
}
if(sharpness == 36 || sharpness == 37){
event.setDamage(7.5);	
}
if(sharpness == 38 || sharpness == 39){
event.setDamage(8.0);	
}
if(sharpness == 40 || sharpness == 41){
event.setDamage(9.0);	
}
if(sharpness == 42 || sharpness == 43){
event.setDamage(10.0);	
}
if(sharpness == 44 || sharpness == 45){
event.setDamage(11.0);	
}
if(sharpness == 46 || sharpness == 47){
event.setDamage(12.0);	
}
if(sharpness == 48 || sharpness == 49){
event.setDamage(14.0);	
}
if(sharpness == 50 || sharpness == 51){
event.setDamage(15.0);	
}
if(sharpness == 52 || sharpness == 53){
event.setDamage(16.0);	
}
if(sharpness == 54 || sharpness == 55){
event.setDamage(17.0);	
}
if(sharpness == 56){
event.setDamage(18.0);	
}
if(h !=null){
this.removeDurability(19,h, p1, p2);	
}
if(c !=null){
this.removeDurability(22,c,p1, p2);	
}
if(l !=null){
this.removeDurability(22,l, p1, p2);	
}
if(b !=null){
this.removeDurability(19,b,p1, p2);	
}	
}
	
}
	

public void removeDurability(int amount,ItemStack is,Player p1,Player p2){
if(is.containsEnchantment(Enchantment.DURABILITY)){
int level = is.getEnchantmentLevel(Enchantment.DURABILITY);
boolean strenght = p2.getActivePotionEffects().contains(PotionEffectType.INCREASE_DAMAGE);
if(strenght){
if(level == 1){
is.setDurability((short)(is.getDurability() + (short)amount - 2 + 4));	
}
if(level == 2){
is.setDurability((short)(is.getDurability() + (short)amount - 4 + 5));	
}
if(level == 3){
is.setDurability((short)(is.getDurability() + (short)amount - 6 + 7));		
}
if(level == 4){
is.setDurability((short)(is.getDurability() + (short)amount - 8 + 9));	
}
if(level == 5){
is.setDurability((short)(is.getDurability() + (short)amount - 10 + 11));	
}
if(level == 6){
is.setDurability((short)(is.getDurability() + (short)amount - 12 + 13));	
}
if(level == 7){
is.setDurability((short)(is.getDurability() + (short)amount - 14 + 15));	
}	
p1.updateInventory();
}
if(!strenght){
if(level == 1){
is.setDurability((short)(is.getDurability() + (short)amount - 2));	
}
if(level == 2){
is.setDurability((short)(is.getDurability() + (short)amount - 4));	
}
if(level == 3){
is.setDurability((short)(is.getDurability() + (short)amount - 6));	
}
if(level == 4){
is.setDurability((short)(is.getDurability() + (short)amount - 8));	
}
if(level == 5){
is.setDurability((short)(is.getDurability() + (short)amount - 10));	
}
if(level == 6){
is.setDurability((short)(is.getDurability() + (short)amount - 12));	
}
if(level == 7){
is.setDurability((short)(is.getDurability() + (short)amount - 14));	
}
p1.updateInventory();
}
}	
}

}
