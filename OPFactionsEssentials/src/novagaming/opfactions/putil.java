package novagaming.opfactions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class putil {
	
private static putil instance = new putil();

public static putil getInstance(){
return instance;	
}

public File dirz;

public void setup(File dir){
if(!dir.exists()){
dir.mkdirs();	
}
dirz = new File(dir + File.separator + "Users");
if(!dirz.exists()){
dirz.mkdirs();	 
}
}

public void sendKillData(CommandSender cs,int page,int removed,int keep){
ArrayList<String> data = this.getKillsData();
Collections.sort(data, new Comparator<String>() {
public int compare(String a, String b) {
int aVal = Integer.parseInt(a.split(" ")[0]);
int bVal = Integer.parseInt(b.split(" ")[0]);
return Integer.compare(aVal, bVal);
}
});
for(int i =0 ; i<removed; i++){
if(data.size() >i){
data.remove(i);	
}
}
cs.sendMessage(ChatColor.GOLD + "TopKills Page"+page);
for (int i = (data.size() > keep ? keep : data.size()) - 1; i >= 0; i--) {
String line = data.get(i);
String player = line.split(" ")[1], bal = line.split(" ")[0];
cs.sendMessage(ChatColor.GOLD+"#"+i+""+ChatColor.YELLOW + " "+ player + " : " + bal);
}		
}

public void sendDeathData(CommandSender cs,int page,int removed,int keep){
ArrayList<String> data = this.getDeathData();
Collections.sort(data, new Comparator<String>() {
public int compare(String a, String b) {
int aVal = Integer.parseInt(a.split(" ")[0]);
int bVal = Integer.parseInt(b.split(" ")[0]);
return Integer.compare(aVal, bVal);
}
});
for(int i =0 ; i<removed; i++){
if(data.size() >i){
data.remove(i);	
}
}
cs.sendMessage(ChatColor.GOLD + "TopDeaths Page"+page);
for (int i = (data.size() > keep ? keep : data.size()) - 1; i >= 0; i--) {
String line = data.get(i);
String player = line.split(" ")[1], bal = line.split(" ")[0];
cs.sendMessage(ChatColor.GOLD+"#"+i+""+ChatColor.YELLOW + " "+ player + " : " + bal);
}		
}

public void sendKDRData(CommandSender cs,int page,int removed,int keep){
ArrayList<String> data = this.getKDRData();
Collections.sort(data, new Comparator<String>() {
public int compare(String a, String b) {
double aVal = Double.parseDouble(a.split(" ")[0]);
double bVal = Double.parseDouble(b.split(" ")[0]);
return Double.compare(aVal,bVal);
}
});
for(int i =0 ; i<removed; i++){
if(data.size() >i){
data.remove(i);	
}
}
cs.sendMessage(ChatColor.GOLD + "TopKDR Page"+page);
for (int i = (data.size() > keep ? keep : data.size()) - 1; i >= 0; i--) {
String line = data.get(i);
String player = line.split(" ")[1], bal = line.split(" ")[0];
cs.sendMessage(ChatColor.GOLD+"#"+i+""+ChatColor.YELLOW + " "+ player + " : " + bal);
}		
}


public ArrayList<String> getKillsData(){
ArrayList<String> result = new ArrayList<String>();
for(File file : new File(dirz.getPath()).listFiles()){
FileConfiguration config = YamlConfiguration.loadConfiguration(file);
result.add(config.getInt("kills") + " " + config.getString("name"));
}
return result;
}

public ArrayList<String> getDeathData(){
ArrayList<String> result = new ArrayList<String>();
for(File file : new File(dirz.getPath()).listFiles()){
FileConfiguration config = YamlConfiguration.loadConfiguration(file);
result.add(config.getInt("deaths") + " " + config.getString("name"));
}
return result;
}

public ArrayList<String> getKDRData(){
ArrayList<String> result = new ArrayList<String>();
for(File file : new File(dirz.getPath()).listFiles()){
FileConfiguration config = YamlConfiguration.loadConfiguration(file);
double kills = config.getDouble("kills");
double deaths = config.getDouble("deaths");
double resultz = 0;
if(deaths == 0){
resultz = kills;	
}else{
resultz = kills / deaths;
}
result.add(resultz + " " + config.getString("name"));

}
return result;
}

public void sendTopKills(CommandSender cs,int page){
int keep = page * 10;
int remove = keep - 10;
this.sendKillData(cs,page,remove, keep);
}

public void sendTopDeaths(CommandSender cs,int page){
int keep = page * 10;
int remove = keep - 10;
this.sendDeathData(cs,page,remove, keep);
}

public void sendTopKDR(CommandSender cs,int page){
int keep = page * 10;
int remove = keep - 10;
this.sendKDRData(cs,page,remove, keep);
}

public void addKill(UUID id,int amount){
File file = new File(dirz + File.separator + id.toString() + ".yml");
FileConfiguration config = YamlConfiguration.loadConfiguration(file);	
config.set("kills",config.getInt("kills")+amount);
try {
config.save(file);
} catch (IOException e) {
}
}

public void addDeath(UUID id,int amount){
File file = new File(dirz + File.separator + id.toString() + ".yml");
FileConfiguration config = YamlConfiguration.loadConfiguration(file);	
config.set("deaths",config.getInt("deaths")+amount);
try {
config.save(file);
} catch (IOException e) {
}
}



public UUID getID(String name){
UUID result = null;
for(File file : new File(dirz.getPath()).listFiles()){
FileConfiguration config = YamlConfiguration.loadConfiguration(file);	
if(config.getString("name").equalsIgnoreCase(name)){
result = UUID.fromString(config.getString("uuid"));
}
}
return result;
}

public boolean registered(UUID id){
File user = new File(dirz+ File.separator + id.toString() + ".yml");
return user.exists();
}

public void createAccount(Player p){
File user = new File(dirz + File.separator + p.getUniqueId() + ".yml");
if(!user.exists()){
FileConfiguration config = YamlConfiguration.loadConfiguration(user);
config.set("name",p.getName());
config.set("uuid",p.getUniqueId().toString());
config.set("kills",0);
config.set("deaths",0);
try {
config.save(user);
} catch (IOException e) {
e.printStackTrace();
}
}
}

public int getInt(String what,UUID id){
File file = new File(dirz + File.separator + id.toString() + ".yml");
FileConfiguration config = YamlConfiguration.loadConfiguration(file);	
return config.getInt(what);
}

public double getKDR(UUID id){
File file = new File(dirz + File.separator + id.toString() + ".yml");
FileConfiguration config = YamlConfiguration.loadConfiguration(file);
double kills = config.getDouble("kills");
double deaths = config.getDouble("deaths");
double result = 0;
if(deaths == 0){
result = kills;	
}else{
result = kills / deaths;
}
return result;
}

public void updateAccount(Player p){
File user = new File(dirz + File.separator + p.getUniqueId() + ".yml");
FileConfiguration config = YamlConfiguration.loadConfiguration(user);
config.set("name",p.getName());
config.set("uuid",p.getUniqueId().toString());
try {
config.save(user);
} catch (IOException e) {
}
}

}
