package novagaming.opfactions;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class main extends JavaPlugin{

public final playerlistener pl = new playerlistener();
	
public void onEnable(){	
PluginManager pm = Bukkit.getPluginManager();
pm.registerEvents(this.pl,this);
putil.getInstance().setup(getDataFolder());
getCommand("kdr").setExecutor(new kdr());
getCommand("kills").setExecutor(new kills());
getCommand("deaths").setExecutor(new deaths());
getCommand("topkills").setExecutor(new topkills());
getCommand("topdeaths").setExecutor(new topdeaths());
getCommand("topkdr").setExecutor(new topkdr());
}

}
