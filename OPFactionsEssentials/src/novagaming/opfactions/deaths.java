package novagaming.opfactions;

import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class deaths implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String lab,String[] args) {
	if(cmd.getName().equalsIgnoreCase("deaths")){
	if(cs instanceof Player){
	Player p = (Player) cs;	
    if(args.length == 0){
    p.sendMessage(ChatColor.GOLD + "You have " + putil.getInstance().getInt("deaths",p.getUniqueId())+" Deaths");
    return false;	
    }
    if(args.length == 1){
    String name = args[0];
    UUID id = putil.getInstance().getID(name);
    if(id == null){
    p.sendMessage(ChatColor.GOLD + "Can't find data for " + name);
    return false;	
    }
    if(id !=null){
    p.sendMessage(ChatColor.GOLD + name+" has " + putil.getInstance().getInt("deaths", id)+" Deaths");
    return false;	
    }
    }
	}	
	}
	return false;
	}

}
