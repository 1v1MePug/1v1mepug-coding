package pvpstats;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.bukkit.plugin.Plugin;

import code.husky.mysql.MySQL;

public class sqlutil {
	
public MySQL mysql;
public Connection c;

private static sqlutil instance = new sqlutil();

public static sqlutil getInstance(){
return instance;	
}

public int getRowNumber(){
int numberRow = 0;
try{
String table = fileutil.getInstance().getMYSQLVariable("table");
String query = "select count(*) from "+table;
PreparedStatement st = c.prepareStatement(query);
ResultSet rs = st.executeQuery();
while(rs.next()){
numberRow = rs.getInt("count(*)");
}
}catch (Exception ex){
System.out.println(ex.getMessage());
}
return numberRow;
}
	
public void createConnection(Plugin p){
String ip = fileutil.getInstance().getMYSQLVariable("ip");
String port = fileutil.getInstance().getMYSQLVariable("port");
String user = fileutil.getInstance().getMYSQLVariable("user");
String pass = fileutil.getInstance().getMYSQLVariable("pass");
String table = fileutil.getInstance().getMYSQLVariable("database");
mysql = new MySQL(p,ip,port,table,user,pass);
try {
c = mysql.openConnection();
} catch (Exception e) {
}
}

}
