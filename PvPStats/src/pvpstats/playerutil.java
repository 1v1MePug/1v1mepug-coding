package pvpstats;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.bukkit.entity.Player;

public class playerutil {
	
	private static playerutil instance = new playerutil();
	
	public static playerutil getInstance(){
	return instance;	
	}
	
	public boolean isRegistered(UUID id){
	boolean result = false;
	if(this.getInt(6, id) == 1){
	result = true;	
	}else{
	result = false;	
	}
	return result;
	}
	
	
	public void register(Player p){
	try {
	String table = fileutil.getInstance().getMYSQLVariable("table");
	PreparedStatement ps = sqlutil.getInstance().c.prepareStatement("INSERT INTO `"+table+"` (UUID, Username, CrateKeys, Kills, Deaths, Registered, RegisterDate, LastLoginDate) VALUES (?, ?, ?, ?, ?, ?, ?, ?);");
	ps.setString(1,p.getUniqueId().toString());
	ps.setString(2,p.getName());
	ps.setInt(3,3);
	ps.setInt(4,0);
	ps.setInt(5,0);
	ps.setInt(6,1);
	ps.setString(7,now("yyyy/MM/dd HH:mm:ss"));
	ps.setString(8,now("yyyy/MM/dd HH:mm:ss"));
	ps.executeUpdate();
	} catch (SQLException e1) {
	}
	}
	
	public String getRegisterDate(String username){
	return this.getString(7, username);	
	}
	public String getLastLoginDate(String username){
	return this.getString(8,username);	
	}
	
	public String now(String format){
    SimpleDateFormat dateFormat = new SimpleDateFormat(format);
    Date date = new Date();
	return dateFormat.format(date);
	}
	
    public void setupTable(String table){
    try{
    Statement statement = sqlutil.getInstance().c.createStatement();	
    statement.execute("CREATE TABLE IF NOT EXISTS `"+table+"` (UUID VARCHAR(36), Username VARCHAR(36), CrateKeys INT(6), Kills INT(6), Deaths INT(6), Registered(6));");
    }catch(Exception e){	
    }
    }
   
    public int getKeys(String Username){
    return this.getInt(3,Username);	
    }
    public int getKills(String Username){
    return this.getInt(4,Username);	
    }
    public int getDeaths(String Username){
    return this.getInt(5,Username);	
    }
    public double getKDR(String username){
    double cal = 0;
    if(this.getDeaths(username) == 0){
    cal = this.getKills(username);
    }else{
    cal = (double)this.getKills(username) /  this.getDeaths(username);
    }
    return cal;
    }
    
    public UUID getUUID(String username){
    return UUID.fromString(this.getString(1,username));	
    }
    
    
    
    public void addKeys(String username,int keys){
    try{
    Statement st = sqlutil.getInstance().c.createStatement();
    String table = fileutil.getInstance().getMYSQLVariable("table");
    int key = this.getInt(3,username) + keys;
    st.executeUpdate("UPDATE "+table+" SET CrateKeys='"+key+"' WHERE Username='"+username+ "';");
    }catch (Exception e){    	
    }	
    }
    
    public void removeKeys(String username,int keys){
    try{
    Statement st = sqlutil.getInstance().c.createStatement();
    String table = fileutil.getInstance().getMYSQLVariable("table");
    int key = this.getInt(3,username) - keys;
    st.executeUpdate("UPDATE "+table+" SET CrateKeys='"+key+"' WHERE Username='"+username+ "';");
    }catch (Exception e){    	
    }	
    }
    
    public void addKill(String username){
    try{
    Statement st = sqlutil.getInstance().c.createStatement();
    String table = fileutil.getInstance().getMYSQLVariable("table");
    int key = this.getInt(4,username) + 1;
    st.executeUpdate("UPDATE "+table+" SET Kills='"+key+"' WHERE Username='"+username+ "';");
    }catch (Exception e){    	
    }	
    }
    
    public void addDeath(String username){
    try{
    Statement st = sqlutil.getInstance().c.createStatement();
    String table = fileutil.getInstance().getMYSQLVariable("table");
    int key = this.getInt(5,username) + 1;
    st.executeUpdate("UPDATE "+table+" SET Deaths='"+key+"' WHERE Username='"+username+ "';");
    }catch (Exception e){    	
    }	
    }
		    
		    
	public void update(UUID id,String username){
    try{
    String table = fileutil.getInstance().getMYSQLVariable("table");
	Statement st = sqlutil.getInstance().c.createStatement();
	String now = now("yyyy/MM/dd HH:mm:ss");
	st.executeUpdate("UPDATE "+table+" SET Username='"+username+"' WHERE UUID='"+id.toString() + "';");
	st.executeUpdate("UPDATE "+table+" SET LastLoginDate='"+now+"' WHERE UUID='"+id.toString() + "';");
	if(this.getRegisterDate(username) == null){
	st.executeUpdate("UPDATE "+table+" SET RegisterDate='"+now+"' WHERE UUID='"+id.toString() + "';");	
	}
	}catch (Exception e){    	
	}
	}
	

	public String getString(int value,UUID id){
	String table = fileutil.getInstance().getMYSQLVariable("table");
	String result = null;
	try {
	Statement statement = sqlutil.getInstance().c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE UUID ='" +id.toString()+ "';");
	res.next();
	result = res.getString(value);
	} catch (SQLException e){
	}
	return result;
	}
	
	public String getString(int value,String id){
	String table = fileutil.getInstance().getMYSQLVariable("table");
	String result = null;
	try {
	Statement statement = sqlutil.getInstance().c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE Username ='" +id.toString()+ "';");
	res.next();
	result = res.getString(value);
	} catch (SQLException e){
	}
	return result;
	}
	
	public int getInt(int value,String id){
	String table = fileutil.getInstance().getMYSQLVariable("table");
	int result = 0;
	try {
	Statement statement = sqlutil.getInstance().c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE Username ='" +id.toString()+ "';");
	res.next();
	result = res.getInt(value);
	} catch (SQLException e){
	}
	return result;
	}
	
	public int getInt(int value,UUID id){
	String table = fileutil.getInstance().getMYSQLVariable("table");
	int result = 0;
	try {
	Statement statement = sqlutil.getInstance().c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE UUID ='" +id.toString()+ "';");
	res.next();
	result = res.getInt(value);
	} catch (SQLException e){
	}
	return result;
	}
	
	public double getDouble(int value,UUID id){
	String table = fileutil.getInstance().getMYSQLVariable("table");
	double result = 0;
	try {
	Statement statement = sqlutil.getInstance().c.createStatement();
	ResultSet res = statement.executeQuery("SELECT * FROM "+table+" WHERE UUID ='" +id.toString()+ "';");
	res.next();
	result = res.getDouble(value);
	} catch (SQLException e){
	}
	return result;
	}

}
