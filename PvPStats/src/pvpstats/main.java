package pvpstats;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import pvpstats.commands.deaths;
import pvpstats.commands.kdr;
import pvpstats.commands.kills;
import pvpstats.commands.stats;
import pvpstats.commands.topdeaths;
import pvpstats.commands.topkdr;
import pvpstats.commands.topkills;
import pvpstats.listeners.join;
import pvpstats.listeners.leave;
import pvpstats.listeners.pvp;

public class main extends JavaPlugin{
	
	
public final join join = new join(); 
public final leave leave = new leave();
public final pvp pvp = new pvp();
	
public void onEnable(){
fileutil.getInstance().setup(getDataFolder());
sqlutil.getInstance().createConnection(this);
playerutil.getInstance().setupTable(fileutil.getInstance().getMYSQLVariable("table"));
PluginManager pm = Bukkit.getServer().getPluginManager();
pm.registerEvents(this.join,this);
pm.registerEvents(this.leave,this);
pm.registerEvents(this.pvp,this);
getCommand("deaths").setExecutor(new deaths());
getCommand("kdr").setExecutor(new kdr());
getCommand("kills").setExecutor(new kills());
getCommand("stats").setExecutor(new stats());
getCommand("topkills").setExecutor(new topkills());
getCommand("topkdr").setExecutor(new topkdr());
getCommand("topdeaths").setExecutor(new topdeaths());
}

}
