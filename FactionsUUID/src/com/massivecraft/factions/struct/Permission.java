package com.massivecraft.factions.struct;

import org.bukkit.command.CommandSender;
import com.massivecraft.factions.P;

public enum Permission
{
	ACCESS("access"),
	ACCESS_ANY("access.any"),
	ACCESS_VIEW("access.view"),
	ADMIN("adminmode"),
	AUTOCLAIM("player"),
	CAPE("cape"),
	CAPE_GET("cape.get"),
	CAPE_SET("cape.set"),
	CAPE_REMOVE("cape.remove"),
	CLAIM("player"),
	CLAIM_RADIUS("player"),
	CONFIG("config"),
	CREATE("player"),
	DEINVITE("player"),
	DEMOTE("player"),
	DESCRIPTION("player"),
	DISBAND("player"),
	FLAG("player"),
	FLAG_SET("player"),
	HELP("player"),
	HOME("player"),
	INVITE("player"),
	JOIN("player"),
	JOIN_ANY("join.any"),
	JOIN_OTHERS("join.others"),
	KICK("player"),
	LEADER("player"),
	LEADER_ANY("leader.any"),
	LEAVE("player"),
	LIST("player"),
	LOCK("lock"),
	MAP("player"),
	MONEY_BALANCE("player"),
	MONEY_BALANCE_ANY("player"),
	MONEY_DEPOSIT("player"),
	MONEY_F2F("player"),
	MONEY_F2P("player"),
	MONEY_P2F("player"),
	MONEY_WITHDRAW("player"),
	OFFICER("player"),
	OFFICER_ANY("player"),
	OPEN("player"),
	PERM("perm"),
	POWER("player"),
	POWER_ANY("player"),
	POWERBOOST("powerboost"),
	PROMOTE("player"),
	RELATION("player"),
	RELOAD("reload"),
	SAVE("save"),
	SEE_CHUNK("seechunk"),
	SETHOME("player"),
	SHOW("player"),
	TAG("player"),
	TITLE("player"),
	UNCLAIM("player"),
	UNCLAIM_ALL("player"),
	VERSION("version"),
	;
	
	public final String node;
	
	Permission(final String node)
	{
		this.node = "factions."+node;
	}
	
	public boolean has(CommandSender sender, boolean informSenderIfNot)
	{
		return P.p.perm.has(sender, this.node, informSenderIfNot);
	}
	
	public boolean has(CommandSender sender)
	{
		return has(sender, false);
	}
}
