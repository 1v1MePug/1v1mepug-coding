package opkits;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class playerlistener implements Listener{
	
public ItemStack getItem(String name,Material material,int amount,short data){
ItemStack is = new ItemStack(material,amount,data);
ItemMeta im = is.getItemMeta();
im.setDisplayName(name);
is.setItemMeta(im);
return is;
}

public void loadInv(Player p){
Inventory inv = Bukkit.createInventory(null,27,"Kit Selector");
inv.addItem(this.getItem("swordsman",Material.IRON_SWORD,1,(short)0));
inv.addItem(this.getItem("axeman",Material.IRON_AXE,1,(short)0));
inv.addItem(this.getItem("archer",Material.BOW,1,(short)0));
p.openInventory(inv);
}

public void giveKit(Player p,String kit){
p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED,Integer.MAX_VALUE,1));
p.getInventory().clear();
p.updateInventory();
PlayerInventory pi = p.getInventory();
if(kit.equalsIgnoreCase("swordsman")){
ItemStack helmet = new ItemStack(Material.IRON_HELMET);
ItemStack chestplate = new ItemStack(Material.IRON_CHESTPLATE);
ItemStack leggings = new ItemStack(Material.IRON_LEGGINGS);
ItemStack boots = new ItemStack(Material.IRON_BOOTS);
ItemStack sword = new ItemStack(Material.IRON_SWORD);
sword.addEnchantment(Enchantment.DAMAGE_ALL,1);
pi.setHelmet(helmet);
pi.setChestplate(chestplate);
pi.setLeggings(leggings);
pi.setBoots(boots);
pi.addItem(sword);
p.updateInventory();
}
if(kit.equalsIgnoreCase("axeman")){
ItemStack helmet = new ItemStack(Material.IRON_HELMET);
ItemStack chestplate = new ItemStack(Material.IRON_CHESTPLATE);
ItemStack leggings = new ItemStack(Material.IRON_LEGGINGS);
ItemStack boots = new ItemStack(Material.IRON_BOOTS);
ItemStack sword = new ItemStack(Material.IRON_AXE);
sword.addEnchantment(Enchantment.DAMAGE_ALL,1);
pi.setHelmet(helmet);
pi.setChestplate(chestplate);
pi.setLeggings(leggings);
pi.setBoots(boots);
pi.addItem(sword);
p.updateInventory();
}
if(kit.equalsIgnoreCase("archer")){
ItemStack helmet = new ItemStack(Material.CHAINMAIL_HELMET);
helmet.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL,1);
ItemStack chestplate = new ItemStack(Material.LEATHER_CHESTPLATE);
chestplate.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL,1);
ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS);
leggings.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL,1);
ItemStack boots = new ItemStack(Material.CHAINMAIL_BOOTS);
boots.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL,1);
ItemStack bow = new ItemStack(Material.BOW);
bow.addEnchantment(Enchantment.ARROW_DAMAGE,1);
bow.addEnchantment(Enchantment.ARROW_FIRE,1);
ItemStack arrow = new ItemStack(Material.ARROW,128);
ItemStack sword = new ItemStack(Material.STONE_SWORD);
sword.addEnchantment(Enchantment.DAMAGE_ALL,1);
pi.setHelmet(helmet);
pi.setChestplate(chestplate);
pi.setLeggings(leggings);
pi.setBoots(boots);
pi.addItem(bow);
pi.addItem(arrow);
pi.addItem(sword);
p.updateInventory();
}
for(int i = 0; i<27; i++){
if(pi.getItem(i) == null){
ItemStack soup = new ItemStack(Material.MUSHROOM_SOUP,1);
pi.setItem(i,soup);
}
}
	
}

@EventHandler
public void onInvClick(InventoryClickEvent event){
Player p = (Player) event.getWhoClicked();
ItemStack is = event.getCurrentItem();
Inventory inv = event.getInventory();
if(inv.getName().equalsIgnoreCase("Kit Selector")){
if(is.hasItemMeta()){
String itemname = is.getItemMeta().getDisplayName().toLowerCase();
if(itemname.equalsIgnoreCase("swordsman")){
this.giveKit(p,itemname);	
p.updateInventory();
}
if(itemname.equalsIgnoreCase("axeman")){
this.giveKit(p,itemname);	
p.updateInventory();
}
if(itemname.equalsIgnoreCase("archer")){
this.giveKit(p,itemname);	
p.updateInventory();
}
event.setCancelled(true);
}
}
}

@EventHandler
public void onDeath(PlayerDeathEvent event){
if(event.getEntity() instanceof Player){
event.getDrops().clear();
}
}

@EventHandler
public void onJoin(PlayerJoinEvent event){
Player p = (Player) event.getPlayer();
p.getInventory().clear();
p.getInventory().addItem(this.getItem("Kit Selector", Material.EMERALD,1,(short)0));
}
@EventHandler
public void onRespawn(PlayerRespawnEvent event){
Player p = (Player) event.getPlayer();
p.getInventory().addItem(this.getItem("Kit Selector", Material.EMERALD,1,(short)0));
}

@EventHandler
public void onPlayerInteract(PlayerInteractEvent event){
Player p = (Player) event.getPlayer();
ItemStack is = event.getItem();
if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK){
if(is.getType() == Material.EMERALD){
if(is.hasItemMeta()){
String name = is.getItemMeta().getDisplayName();
if(name.equalsIgnoreCase("Kit Selector")){
this.loadInv(p);
}
}		
}
if(is.getType() == Material.MUSHROOM_SOUP){
Damageable dam = (Damageable) p;
if(dam.getHealth() < 20.0){
if (dam.getHealth() >=dam.getMaxHealth()) {
return;
}
double amount = 4.5;
dam.setHealth(dam.getHealth() + amount > dam.getMaxHealth() ? dam.getMaxHealth() : dam.getHealth() + amount);
is.setType(Material.AIR);
event.setCancelled(true);
PlayerInventory pi = p.getInventory();
pi.setItemInHand(null);
p.updateInventory();
}
}
}
}
	
	
}
