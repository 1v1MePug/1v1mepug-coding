package net.novagaming;

import net.novagaming.commands.near;
import net.novagaming.commands.ping;
import net.novagaming.commands.renameitem;
import net.novagaming.commands.wild;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class main extends JavaPlugin{

	
public void onEnable(){
fileutil.getInstance().setup(this);	
perms.getInstance().setup(Bukkit.getServer().getPluginManager());
getCommand("near").setExecutor(new near());
getCommand("ping").setExecutor(new ping());
getCommand("renameitem").setExecutor(new renameitem());
getCommand("wild").setExecutor(new wild());
}


}