package net.novagaming.commands;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ping implements CommandExecutor {
	
  public static int getPlayerPing(Player player){
  int ping = 0;
  try{
  Class<?> craftPlayer = Class.forName("org.bukkit.craftbukkit."+getServerVersion() + ".entity.CraftPlayer");
  Object converted = craftPlayer.cast(player);
  Method handle = converted.getClass().getMethod("getHandle", new Class[0]);
  Object entityPlayer = handle.invoke(converted, new Object[0]);
  Field pingField = entityPlayer.getClass().getField("ping");
  ping = pingField.getInt(entityPlayer);
  }catch(Exception e){  
  }
  return ping;
  }

  public static String getServerVersion(){
  Pattern brand = Pattern.compile("(v|)[0-9][_.][0-9][_.][R0-9]*");
  String pkg = Bukkit.getServer().getClass().getPackage().getName();
  String version = pkg.substring(pkg.lastIndexOf('.') + 1);
  if (!brand.matcher(version).matches()){
  version = "";
  }
  return version;
  }

  @Override
  public boolean onCommand(CommandSender cs, Command cmd, String lab,String[] args) {
  if(cmd.getName().equalsIgnoreCase("ping")){
  if(cs instanceof Player){
  Player p =  (Player) cs; 
  if(args.length == 0){
  p.sendMessage(ChatColor.GOLD + "Ping : "+getPlayerPing(p) + "ms");
  return false;  
  }
  if(args.length == 1){
  String name = args[0];
  Player target = (Player) Bukkit.getServer().getPlayer(name);
  if(target !=null){
  p.sendMessage(ChatColor.GOLD + target.getDisplayName() + "'s Ping : "+getPlayerPing(target) + "ms");
  return false; 
  }
  if(target == null){
  p.sendMessage(ChatColor.GOLD + "Error : Player not online");
  return false; 
  }
  }
  
  }
  }
  return false;
 }

}
