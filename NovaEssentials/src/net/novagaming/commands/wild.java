package net.novagaming.commands;

import java.util.Random;

import net.novagaming.fileutil;
import net.novagaming.perms;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class wild implements CommandExecutor{

@Override
public boolean onCommand(CommandSender cs, Command cmd, String lab,String[] args) {
if(cmd.getName().equalsIgnoreCase("wild")){
if(cs instanceof Player){
Player p = (Player) cs;	
if(!p.hasPermission(perms.wild)){
p.sendMessage(ChatColor.RED + "You don't have permission");
return false;	
}
if(p.hasPermission(perms.wild)){
Random r = new Random();
World w = Bukkit.getServer().getWorld(fileutil.getInstance().getStringValue("randomtpworld"));
int x = r.nextInt(fileutil.getInstance().getIntValue("randomtpxrange"));
int z = r.nextInt(fileutil.getInstance().getIntValue("randomtpzrange"));
int y = w.getHighestBlockYAt(x,z);
Location loc = new Location(w,x,y,z);
p.teleport(loc);
p.sendMessage(ChatColor.GOLD + "You have been teleported to X:" + x + " Y:"+y + " Z:" + z);
}
}
}
return false;
}

}
