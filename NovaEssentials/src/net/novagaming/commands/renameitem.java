package net.novagaming.commands;

import net.novagaming.perms;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.ItemMeta;

public class renameitem implements CommandExecutor{

@Override
public boolean onCommand(CommandSender cs, Command cmd, String lab,String[] args) {
if(cmd.getName().equalsIgnoreCase("renameitem")){
if(cs instanceof Player){
Player p = (Player) cs;	
if(!p.hasPermission(perms.renameitem)){
p.sendMessage(ChatColor.RED + "You don't have permission");
return false;	
}
if(p.hasPermission(perms.renameitem)){
StringBuilder builder = new StringBuilder();
for(int i = 0; i<args.length; i++){
builder.append(args[i] + " ");	
}
ItemMeta im = p.getItemInHand().getItemMeta();
im.setDisplayName(builder.toString().replace('&','�'));
p.getItemInHand().setItemMeta(im);
p.sendMessage(ChatColor.GOLD + "You changed your itemname to " + builder.toString().replace('&','�'));
}
}
}
return false;
}

}
