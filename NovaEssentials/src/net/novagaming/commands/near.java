package net.novagaming.commands;

import net.novagaming.fileutil;
import net.novagaming.perms;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class near implements CommandExecutor{

public void sendNearbyPlayers(final Player p,double radius){
StringBuilder builder = new StringBuilder();
for(Entity e : p.getNearbyEntities(radius, radius, radius)){
if(e instanceof Player){
Player p2 = (Player) e;
builder.append(p2.getDisplayName() + "["+p.getLocation().distance(p2.getLocation())+"]" + " ");	
}
}
p.sendMessage(ChatColor.GOLD + "Nearby players: "+builder.toString());	
}
	
	
@Override
public boolean onCommand(CommandSender cs, Command cmd, String arg2,String[] arg3) {
if(cmd.getName().equalsIgnoreCase("near")){
if(cs instanceof Player){
Player p = (Player) cs;
if(!p.hasPermission(perms.near)){
p.sendMessage(ChatColor.RED + "You don't have permission");
return false;	
}
if(p.hasPermission(perms.near)){
this.sendNearbyPlayers(p,fileutil.getInstance().getIntValue("nearradius"));
return false;	
}
}
}
return false;
}

}
