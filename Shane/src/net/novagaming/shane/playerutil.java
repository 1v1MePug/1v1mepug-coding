package net.novagaming.shane;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class playerutil {
	
private static playerutil instance = new playerutil();

public static playerutil getInstance(){
return instance;	
}

public boolean completedAchievement(UUID id,String achievement){
File playerfile = new File(fileutil.getInstance().playerdir + File.separator + id.toString() + ".yml");	
FileConfiguration config = YamlConfiguration.loadConfiguration(playerfile);
return config.getBoolean(achievement);
}

public void completeAchievement(UUID id,String username,String achievement){
File playerfile = new File(fileutil.getInstance().playerdir + File.separator + id.toString() + ".yml");	
FileConfiguration config = YamlConfiguration.loadConfiguration(playerfile);
config.set(achievement,true);
try {
config.save(playerfile);
} catch (IOException e) {
}
String msg = fileutil.getInstance().getStringValue("achievementcompletemsg")
.replaceAll("{USER}",username)
.replaceAll("{ACHIEVEMENT}",achievement);
Bukkit.broadcastMessage(msg);
}

public boolean isRegistered(UUID id){
File playerfile = new File(fileutil.getInstance().playerdir + File.separator + id.toString() + ".yml");
return playerfile.exists();
}

public void register(UUID id,String username){
File playerfile = new File(fileutil.getInstance().playerdir + File.separator + id.toString() + ".yml");
FileConfiguration config = YamlConfiguration.loadConfiguration(playerfile);
config.set("Username",username);
try {
config.save(playerfile);
} catch (IOException e) {
}
}

}
