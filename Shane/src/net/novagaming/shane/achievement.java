package net.novagaming.shane;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.gmail.nossr50.datatypes.player.McMMOPlayer;
import com.gmail.nossr50.datatypes.skills.SkillType;
import com.gmail.nossr50.util.player.UserManager;

public class achievement implements CommandExecutor{
	
	public static int getAllSkilllevels(String player){
	int count = 0;
	for(SkillType skill : SkillType.values()){
	count += getMCMMOSkillLevel(skill,player);	
	}
	return count;	
	}
	
	public static int getMCMMOSkillLevel(SkillType st, String player) {
	    final McMMOPlayer mPlayer = UserManager.getPlayer(player);
	    if (mPlayer == null) {
	    return -1;
	    }
	    return mPlayer.getProfile().getSkillLevel(st);
	    }

@Override
public boolean onCommand(CommandSender cs, Command cmd, String lab,String[] args) {
if(cmd.getName().equalsIgnoreCase("achievements")){
if(cs instanceof Player){
Player p = (Player) cs;
UUID id = p.getUniqueId();
Inventory inv = Bukkit.createInventory(null,27,fileutil.getInstance().getStringValue("achievementinvtitle"));
boolean k = playerutil.getInstance().completedAchievement(id,"1kmcmmolevels");
boolean kk = playerutil.getInstance().completedAchievement(id,"3kmcmmolevels");
boolean kkk = playerutil.getInstance().completedAchievement(id,"5kmcmmolevels");
if(k){
ItemStack is = new ItemStack(Material.WOOL,1,(short)5);
ItemMeta im = is.getItemMeta();
im.setDisplayName(ChatColor.WHITE + "1kmcmmolevels");
ArrayList<String> lore1 = new ArrayList<String>();
lore1.add("Completed");
im.setLore(lore1);
is.setItemMeta(im);
inv.addItem(is);
}else{
ItemStack is = new ItemStack(Material.WOOL,1,(short)14);
ItemMeta im = is.getItemMeta();
im.setDisplayName(ChatColor.WHITE + "1kmcmmolevels");
ArrayList<String> lore1 = new ArrayList<String>();
lore1.add(getAllSkilllevels(p.getName()) + " / 1000");
lore1.add("Not Completed");
im.setLore(lore1);	
is.setItemMeta(im);
inv.addItem(is);
}
if(kk){
ItemStack is = new ItemStack(Material.WOOL,1,(short)5);
ItemMeta im = is.getItemMeta();
im.setDisplayName(ChatColor.WHITE + "3kmcmmolevels");
ArrayList<String> lore1 = new ArrayList<String>();
lore1.add("Completed");
im.setLore(lore1);
is.setItemMeta(im);
inv.addItem(is);
}else{
ItemStack is = new ItemStack(Material.WOOL,1,(short)14);
ItemMeta im = is.getItemMeta();
im.setDisplayName(ChatColor.WHITE + "3kmcmmolevels");
ArrayList<String> lore1 = new ArrayList<String>();
lore1.add(getAllSkilllevels(p.getName()) + " / 3000");
lore1.add("Not Completed");
im.setLore(lore1);	
is.setItemMeta(im);
inv.addItem(is);
}
if(kkk){
ItemStack is = new ItemStack(Material.WOOL,1,(short)5);
ItemMeta im = is.getItemMeta();
im.setDisplayName(ChatColor.WHITE + "5kmcmmolevels");
ArrayList<String> lore1 = new ArrayList<String>();
lore1.add("Completed");
im.setLore(lore1);
is.setItemMeta(im);
inv.addItem(is);
}else{
ItemStack is = new ItemStack(Material.WOOL,1,(short)14);
ItemMeta im = is.getItemMeta();
im.setDisplayName(ChatColor.WHITE + "5kmcmmolevels");
ArrayList<String> lore1 = new ArrayList<String>();
lore1.add(getAllSkilllevels(p.getName()) + " / 5000");
lore1.add("Not Completed");
im.setLore(lore1);	
is.setItemMeta(im);
inv.addItem(is);
}
p.openInventory(inv);
}
}
return false;
}

}
