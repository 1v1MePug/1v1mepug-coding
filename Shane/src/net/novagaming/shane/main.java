package net.novagaming.shane;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class main extends JavaPlugin{
	
public static Economy economy;
public final playerlistener pl = new playerlistener();
	
public void onEnable(){
fileutil.getInstance().setup(getDataFolder());	
PluginManager pm = Bukkit.getPluginManager();
pm.registerEvents(this.pl,this);
if(!setupEconomy()){
this.setEnabled(false);	
}
getCommand("achievements").setExecutor(new achievement());
}

private boolean setupEconomy(){
RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
if (economyProvider != null) {
economy = economyProvider.getProvider();
}
return (economy != null);
}

}
