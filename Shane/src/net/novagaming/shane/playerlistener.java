package net.novagaming.shane;

import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;

import com.gmail.nossr50.datatypes.player.McMMOPlayer;
import com.gmail.nossr50.datatypes.skills.SkillType;
import com.gmail.nossr50.events.experience.McMMOPlayerLevelUpEvent;
import com.gmail.nossr50.util.player.UserManager;

public class playerlistener implements Listener{
	
	public static int getMCMMOSkillLevel(SkillType st, String player) {
    final McMMOPlayer mPlayer = UserManager.getPlayer(player);
    if (mPlayer == null) {
    return -1;
    }
    return mPlayer.getProfile().getSkillLevel(st);
    }
	
@EventHandler
public void onJoin(PlayerJoinEvent event){
Player p = (Player) event.getPlayer();
UUID id = p.getUniqueId();
if(!playerutil.getInstance().isRegistered(id)){
playerutil.getInstance().register(id,p.getName());	
}
}

@EventHandler
public void onInvClick(InventoryClickEvent event){
Inventory inv = event.getInventory();
if(inv.getName().equalsIgnoreCase(fileutil.getInstance().getStringValue("achievementinvtitle"))){
event.setCancelled(true);	
}
}
	
@EventHandler
public void onMCMMO(McMMOPlayerLevelUpEvent event){
int count = 0;
for(SkillType type : SkillType.values()){
count +=getMCMMOSkillLevel(type, event.getPlayer().getName());
}
event.getPlayer().sendMessage(ChatColor.GOLD + "Current McMMO levels :  " + count);
UUID id = event.getPlayer().getUniqueId();
String username = event.getPlayer().getName();
if(count == 1000){
playerutil.getInstance().completeAchievement(id, username,"1kmcmmolevels");	
main.economy.depositPlayer(event.getPlayer(),fileutil.getInstance().getIntValue("1kmcmmoreward"));
}
if(count == 3000){
playerutil.getInstance().completeAchievement(id, username,"3kmcmmolevels");	
main.economy.depositPlayer(event.getPlayer(),fileutil.getInstance().getIntValue("3kmcmmoreward"));
}
if(count == 5000){
playerutil.getInstance().completeAchievement(id, username,"5kmcmmolevels");	
main.economy.depositPlayer(event.getPlayer(),fileutil.getInstance().getIntValue("5kmcmmoreward"));
}


}

}
