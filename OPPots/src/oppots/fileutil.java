package oppots;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class fileutil {
	
private static fileutil instance = new fileutil();


public static fileutil getInstance(){
return instance;	
}

public String debuffsigntitle;
public String armorsigntitle;
public String instanthealthsigntitle;

public File config;
public File playerdata;

public int fireresistanceamount;
public int speedamount;
public int strenghtamount;
public int weaknessamount;
public int instanthealthamount;
public int poisonamount;
public int slownessamount;

public ItemStack slownesspot;
public ItemStack poisonpot;
public ItemStack strenghtpot;
public ItemStack speedpot;
public ItemStack firepot;
public ItemStack weaknesspot;
public ItemStack instanthealthpot;

public ItemStack arrow;

public ItemStack helmet;
public ItemStack chestplate;
public ItemStack leggings;
public ItemStack boots;

public ItemStack sword;
public ItemStack axe;
public ItemStack bow;

public ItemStack steak;
public ItemStack potato;

public boolean doesKitExist(UUID id,int kit){
File file = new File(playerdata + File.separator + id.toString() + kit + ".yml");	
return file.exists();
}

public void loadKit(Player p,int kit){
UUID id = p.getUniqueId();
PlayerInventory pi = p.getInventory();
File file = new File(playerdata + File.separator + id.toString() + kit + ".yml");
FileConfiguration config = YamlConfiguration.loadConfiguration(file);
for(int i = 0; i<37; i++){
if(config.contains("item"+i)){
String split [] = config.getString("item"+i).split("-");
int materialid = Integer.parseInt(split[0]);
int amount = Integer.parseInt(split[1]);
short data = Short.parseShort(split[2]);
ItemStack is = new ItemStack(materialid,amount,data);
if(split.length >= 5){
int idenchant = Integer.parseInt(split[3]);
int level = Integer.parseInt(split[4]);
is.addEnchantment(Enchantment.getById(idenchant), level);	
}
if(split.length >= 7){
int idenchant = Integer.parseInt(split[5]);
int level = Integer.parseInt(split[6]);
is.addEnchantment(Enchantment.getById(idenchant), level);	
}
if(split.length >= 9){
int idenchant = Integer.parseInt(split[7]);
int level = Integer.parseInt(split[8]);
is.addEnchantment(Enchantment.getById(idenchant), level);	
}
if(p.getInventory().firstEmpty() != -1){
pi.addItem(is);
p.updateInventory();
}
}
}
}

public String getEnchantments(ItemStack is){
StringBuilder ez = new StringBuilder();
for(Enchantment ec : is.getEnchantments().keySet()){
ez.append(ec.getId()+"-"+is.getEnchantmentLevel(ec)+"-");	
}
return ez.toString();
}


public void addItemToKit(UUID id,int kit,ItemStack is){
File file = new File(playerdata + File.separator + id.toString() + kit + ".yml");
FileConfiguration config = YamlConfiguration.loadConfiguration(file);
for(int i = 0; i<37; i++){
if(!config.contains("item"+i)){
short data = 0;
data = is.getDurability();
config.set("item"+i,is.getTypeId()+"-"+is.getAmount()+"-"+data+"-"+this.getEnchantments(is));
try {
config.save(file);
} catch (IOException e) {
e.printStackTrace();
}	
return;
}
}		
}

public void saveKit(UUID id,int kit){
File file = new File(playerdata + File.separator + id.toString() + kit + ".yml");
FileConfiguration config = YamlConfiguration.loadConfiguration(file);
try {
config.save(file);
} catch (IOException e) {
}
}

public String getConfigMessage(String configmessage){
FileConfiguration conf = YamlConfiguration.loadConfiguration(config);
return conf.getString(configmessage).replace('&','�');
}

public int getConfigInt(String configint){
FileConfiguration conf = YamlConfiguration.loadConfiguration(config);
return conf.getInt(configint);
}

public void setup(File dir){
try{
if(!dir.exists()){
dir.mkdirs();	
}
config = new File(dir + File.separator + "conf.yml");
if(!config.exists()){
FileConfiguration conf = YamlConfiguration.loadConfiguration(config);
conf.set("error-kit-doesent-exist","&cError : kit doesen't exist");
conf.set("saved-kit","&6Saved kit : <kit>");
conf.set("debuff-sign-title","Debuffs");
conf.set("armor-sign-title","Armor");
conf.set("instanthealth-sign-title","HealthPots");
conf.set("debuff-title","&cDebuffs ");
conf.set("armor-title","&cArmor ");
conf.set("instanthealth-title","&cHealth ");
conf.set("debuff-inv-slots",27);
conf.set("armor-inv-slots",27);
conf.set("instanthealth-inv-slots",27);
conf.set("armor-helmet-materialid",310);
conf.set("armor-chestplate-materialid",311);
conf.set("armor-leggings-materialid",312);
conf.set("armor-boots-materialid",313);
conf.set("armor-helmet-protection-level",1);
conf.set("armor-chestplate-protection-level",1);
conf.set("armor-leggings-protection-level",1);
conf.set("armor-boots-protection-level",1);
conf.set("armor-helmet-unbreaking-level",3);
conf.set("armor-chestplate-unbreaking-level",3);
conf.set("armor-leggings-unbreaking-level",3);
conf.set("armor-boots-unbreaking-level",3);
conf.set("sword-materialid",276);
conf.set("axe-materialid",279);
conf.set("bow-materialid",261);
conf.set("sword-sharpness-level",1);
conf.set("axe-sharpness-level",1);
conf.set("bow-power-level",3);
conf.set("sword-fireaspect-level",1);
conf.set("axe-fireaspect-level",1);
conf.set("bow-flame-level",1);
conf.set("bow-infinity-level",1);
conf.set("sword-unbreaking-level",3);
conf.set("axe-unbreaking-level",3);
conf.set("bow-unbreaking-level",3);
conf.set("arrow-amount",1);
conf.set("steak-amount",64);
conf.set("bakedpotato-amount",64);
conf.set("instanthealth-potion-materialdata",(int)16421);
conf.set("instanthealth-potion-amount",27);
conf.set("speed-potion-materialdata",(int)8226);
conf.set("speed-potion-amount",3);
conf.set("strenght-potion-materialdata",(int)8233);
conf.set("strenght-potion-amount",0);
conf.set("fireresistance-potion-materialdata",(int)8259);
conf.set("fireresistance-potion-amount",3);
conf.set("weakness-potion-materialdata",(int)16456);
conf.set("weakness-potion-amount",3);
conf.set("poison-potion-materialdata",(int)16420);
conf.set("poison-potion-amount",3);
conf.set("slowness-potion-materialdata",(int)16426);
conf.set("slowness-potion-amount",3);
conf.save(config);
}
playerdata = new File(dir + File.separator + "Players");
if(!playerdata.exists()){
playerdata.mkdirs();	
}
}catch(Exception e){
e.printStackTrace();	
}
this.speedamount = this.getConfigInt("speed-potion-amount");
this.strenghtamount = this.getConfigInt("strenght-potion-amount");
this.fireresistanceamount = this.getConfigInt("fireresistance-potion-amount");
this.instanthealthamount = this.getConfigInt("instanthealth-potion-amount");
this.weaknessamount = this.getConfigInt("weakness-potion-amount");
this.poisonamount = this.getConfigInt("poison-potion-amount");
this.slownessamount = this.getConfigInt("slowness-potion-amount");

this.slownesspot = this.createItem(Material.POTION,this.fromInt(this.getConfigInt("slowness-potion-materialdata")));
this.poisonpot = this.createItem(Material.POTION,this.fromInt(this.getConfigInt("poison-potion-materialdata")));
this.strenghtpot = this.createItem(Material.POTION,this.fromInt(getConfigInt("strenght-potion-materialdata")));
this.speedpot = this.createItem(Material.POTION,this.fromInt(getConfigInt("speed-potion-materialdata")));
this.firepot = this.createItem(Material.POTION,this.fromInt(getConfigInt("fireresistance-potion-materialdata")));
this.weaknesspot = this.createItem(Material.POTION,this.fromInt(getConfigInt("weakness-potion-materialdata")));
this.instanthealthpot = this.createItem(Material.POTION,this.fromInt(this.getConfigInt("instanthealth-potion-materialdata")));

this.helmet = this.getHelmetFromConfig();
this.chestplate = this.getChestplateFromConfig();
this.leggings = this.getLeggingsFromConfig();
this.boots = this.getBootsFromConfig();

this.arrow = this.createItem(Material.ARROW,this.getConfigInt("arrow-amount"));
this.sword = this.getSwordFromConfig();
this.axe = this.getAxeFromConfig();
this.bow = this.getBowFromConfig();

this.debuffsigntitle = this.getConfigMessage("debuff-sign-title");
this.armorsigntitle = this.getConfigMessage("armor-sign-title");
this.instanthealthsigntitle = this.getConfigMessage("instanthealth-sign-title");

this.steak = this.createItem(Material.COOKED_BEEF,this.getConfigInt("steak-amount"));
this.potato = this.createItem(Material.BAKED_POTATO,this.getConfigInt("bakedpotato-amount"));
}

public short fromInt(int i){
return (short)i;	
}

public ItemStack createItem(Material material,short data){
ItemStack is = new ItemStack(material,1,data);
return is;
}

public ItemStack createItem(Material material,int amount){
ItemStack is = new ItemStack(material,amount);
return is;
}


public ItemStack getArrowFromConfig(){
int amount = this.getConfigInt("arrow-amount");
ItemStack is = new ItemStack(Material.ARROW,amount);
return is;
}

public ItemStack getSwordFromConfig(){
Material material = Material.getMaterial(this.getConfigInt("sword-materialid"));
int sharp = this.getConfigInt("sword-sharpness-level");
int fireaspect = this.getConfigInt("sword-fireaspect-level");
int unbreaking = this.getConfigInt("sword-unbreaking-level");
ItemStack is = new ItemStack(material,1);
if(sharp >=1){
is.addUnsafeEnchantment(Enchantment.DAMAGE_ALL,sharp);	
}
if(fireaspect >=1){
is.addUnsafeEnchantment(Enchantment.FIRE_ASPECT,fireaspect);	
}
if(unbreaking >=1){
is.addUnsafeEnchantment(Enchantment.DURABILITY,unbreaking);	
}
return is;
}

public ItemStack getAxeFromConfig(){
Material material = Material.getMaterial(this.getConfigInt("axe-materialid"));
int sharp = this.getConfigInt("axe-sharpness-level");
int fireaspect = this.getConfigInt("axe-fireaspect-level");
int unbreaking = this.getConfigInt("axe-unbreaking-level");
ItemStack is = new ItemStack(material,1);
if(sharp >=1){
is.addUnsafeEnchantment(Enchantment.DAMAGE_ALL,sharp);	
}
if(fireaspect >=1){
is.addUnsafeEnchantment(Enchantment.FIRE_ASPECT,fireaspect);	
}
if(unbreaking >=1){
is.addUnsafeEnchantment(Enchantment.DURABILITY,unbreaking);	
}
return is;
}

public ItemStack getBowFromConfig(){
Material material = Material.getMaterial(this.getConfigInt("bow-materialid"));
int power = this.getConfigInt("bow-power-level");
int flame = this.getConfigInt("bow-flame-level");
int unbreaking = this.getConfigInt("bow-unbreaking-level");
int infinity = this.getConfigInt("bow-infinity-level");
ItemStack is = new ItemStack(material,1);
if(power >=1){
is.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE,power);	
}
if(flame >=1){
is.addUnsafeEnchantment(Enchantment.ARROW_FIRE,flame);	
}
if(unbreaking >=1){
is.addUnsafeEnchantment(Enchantment.DURABILITY,unbreaking);	
}
if(infinity >=1){
is.addEnchantment(Enchantment.ARROW_INFINITE,infinity);	
}
return is;
}

public ItemStack getHelmetFromConfig(){
Material material = Material.getMaterial(this.getConfigInt("armor-helmet-materialid"));
int protection = this.getConfigInt("armor-helmet-protection-level");
int unbreaking = this.getConfigInt("armor-helmet-unbreaking-level");
ItemStack is = new ItemStack(material,1);
if(protection >=1){
is.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL,protection);	
}
if(unbreaking >=1){
is.addUnsafeEnchantment(Enchantment.DURABILITY,unbreaking);	
}
return is;
}

public ItemStack getChestplateFromConfig(){
Material material = Material.getMaterial(this.getConfigInt("armor-chestplate-materialid"));
int protection = this.getConfigInt("armor-chestplate-protection-level");
int unbreaking = this.getConfigInt("armor-chestplate-unbreaking-level");
ItemStack is = new ItemStack(material,1);
if(protection >=1){
is.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL,protection);	
}
if(unbreaking >=1){
is.addUnsafeEnchantment(Enchantment.DURABILITY,unbreaking);	
}
return is;
}

public ItemStack getLeggingsFromConfig(){
Material material = Material.getMaterial(this.getConfigInt("armor-leggings-materialid"));
int protection = this.getConfigInt("armor-leggings-protection-level");
int unbreaking = this.getConfigInt("armor-leggings-unbreaking-level");
ItemStack is = new ItemStack(material,1);
if(protection >=1){
is.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL,protection);	
}
if(unbreaking >=1){
is.addUnsafeEnchantment(Enchantment.DURABILITY,unbreaking);	
}
return is;
}

public ItemStack getBootsFromConfig(){
Material material = Material.getMaterial(this.getConfigInt("armor-boots-materialid"));
int protection = this.getConfigInt("armor-boots-protection-level");
int unbreaking = this.getConfigInt("armor-boots-unbreaking-level");
ItemStack is = new ItemStack(material,1);
if(protection >=1){
is.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL,protection);	
}
if(unbreaking >=1){
is.addUnsafeEnchantment(Enchantment.DURABILITY,unbreaking);	
}
return is;
}



}
