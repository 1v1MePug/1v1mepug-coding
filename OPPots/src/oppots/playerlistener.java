package oppots;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public class playerlistener implements Listener{
	
public ItemStack getItem(String name,Material material,int amount,short data){
ItemStack is = new ItemStack(material,amount,data);
ItemMeta im = is.getItemMeta();
im.setDisplayName(name);
is.setItemMeta(im);
return is;
}

@EventHandler
public void onDeath(PlayerDeathEvent event){
if(event.getEntity() instanceof Player){
event.getDrops().clear();
}
}

@EventHandler
public void onDrop(PlayerDropItemEvent event){
event.getItemDrop().remove();	
}

@EventHandler
public void onPlayerInteract(PlayerInteractEvent event){
Player p = (Player) event.getPlayer();
if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK){
BlockState bs = event.getClickedBlock().getState();
if(bs instanceof Sign){
Sign s = (Sign) bs;
String name = s.getLine(0);
if(ChatColor.stripColor(name).equalsIgnoreCase(fileutil.getInstance().debuffsigntitle)){
String invtitle = fileutil.getInstance().getConfigMessage("debuff-title");
int slots = fileutil.getInstance().getConfigInt("debuff-inv-slots");
Inventory inv = Bukkit.createInventory(null,slots,invtitle);
for(int speed = 0; speed<fileutil.getInstance().speedamount; speed++){
inv.setItem(inv.firstEmpty(),fileutil.getInstance().speedpot);	
}
for(int strenght = 0; strenght<fileutil.getInstance().strenghtamount; strenght++){
inv.setItem(inv.firstEmpty(),fileutil.getInstance().strenghtpot);	
}
for(int fire = 0; fire<fileutil.getInstance().fireresistanceamount; fire++){
inv.setItem(inv.firstEmpty(),fileutil.getInstance().firepot);
}
for(int weakness = 0; weakness<fileutil.getInstance().weaknessamount; weakness++){
inv.setItem(inv.firstEmpty(),fileutil.getInstance().weaknesspot);
}
for(int poison = 0; poison<fileutil.getInstance().poisonamount;poison++){
inv.setItem(inv.firstEmpty(),fileutil.getInstance().poisonpot);	
}
for(int slowness = 0; slowness<fileutil.getInstance().slownessamount; slowness++){
inv.setItem(inv.firstEmpty(),fileutil.getInstance().slownesspot);	
}
p.openInventory(inv);
}
if(ChatColor.stripColor(name).equalsIgnoreCase(fileutil.getInstance().armorsigntitle)){
String invtitle = fileutil.getInstance().getConfigMessage("armor-title");
int slots = fileutil.getInstance().getConfigInt("armor-inv-slots");
Inventory inv = Bukkit.createInventory(null,slots,invtitle);
inv.addItem(fileutil.getInstance().helmet);
inv.addItem(fileutil.getInstance().chestplate);
inv.addItem(fileutil.getInstance().leggings);
inv.addItem(fileutil.getInstance().boots);
inv.addItem(fileutil.getInstance().sword);
inv.addItem(fileutil.getInstance().axe);
inv.addItem(fileutil.getInstance().bow);
inv.addItem(fileutil.getInstance().arrow);
inv.addItem(fileutil.getInstance().steak);
inv.addItem(fileutil.getInstance().potato);
p.openInventory(inv);
}
if(ChatColor.stripColor(name).equalsIgnoreCase(fileutil.getInstance().instanthealthsigntitle)){
String invtitle = fileutil.getInstance().getConfigMessage("instanthealth-title");
int slots = fileutil.getInstance().getConfigInt("instanthealth-inv-slots");
Inventory inv = Bukkit.createInventory(null,slots,invtitle);
int amount = fileutil.getInstance().getConfigInt("instanthealth-potion-amount");
for(int i = 0; i<amount; i++){
inv.setItem(i,fileutil.getInstance().instanthealthpot);	
}
p.openInventory(inv);
}
UUID id = p.getUniqueId();
if(name.equalsIgnoreCase("[LoadKit]")){
int kit = Integer.parseInt(s.getLine(1));
if(!fileutil.getInstance().doesKitExist(id, kit)){
p.sendMessage(fileutil.getInstance().getConfigMessage("error-kit-doesent-exist"));
return;	
}
if(fileutil.getInstance().doesKitExist(id, kit)){
fileutil.getInstance().loadKit(p, kit);
}
}
if(name.equalsIgnoreCase("[SaveKit]")){
int kit = Integer.parseInt(s.getLine(1));	
PlayerInventory pi = p.getInventory();
fileutil.getInstance().saveKit(p.getUniqueId(),kit);
String savedkit = fileutil.getInstance().getConfigMessage("saved-kit");
savedkit = savedkit.replace("<kit>",""+kit);
p.sendMessage(savedkit);
for(ItemStack is : pi.getContents()){
if(is !=null){
fileutil.getInstance().addItemToKit(p.getUniqueId(), kit, is);	
}
}
return;
}
}
}
}
	
	
}
